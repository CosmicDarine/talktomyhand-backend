#!/usr/bin/env bash

mvn clean package install

cp ./target/*.jar ./docker/images/talktomyhand/artifact

docker login --username=$DOCKER_USRNAME --password=$DOCKER_PSWD

echo "Update and push talktomyhand database"
cd docker/images/mysql/
docker build -t cosmicdarine/talktomyhand-ddb:latest .
docker push cosmicdarine/talktomyhand-ddb:latest

echo "Update and push talktomyhand app"
cd ../talktomyhand/
docker build -t cosmicdarine/talktomyhand-app:latest .
docker push cosmicdarine/talktomyhand-app:latest
