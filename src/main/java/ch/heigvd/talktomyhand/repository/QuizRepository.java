package ch.heigvd.talktomyhand.repository;

import ch.heigvd.talktomyhand.model.quiz.Quiz;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface QuizRepository extends JpaRepository<Quiz, Long> {
    Optional<Quiz> findByName(String name);
    List<Quiz> findAll();
    boolean existsByName(String name);
    void deleteById(long id);
}
