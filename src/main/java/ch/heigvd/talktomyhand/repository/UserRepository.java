package ch.heigvd.talktomyhand.repository;

import ch.heigvd.talktomyhand.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	Optional<User> findByUsername(String username);

	Optional<User> findByEmail(String email);

	void deleteByUsername(String username);

	Boolean existsByUsername(String username);

	Boolean existsByEmail(String email);

}
