package ch.heigvd.talktomyhand.repository;

import ch.heigvd.talktomyhand.model.user.role.ERole;
import ch.heigvd.talktomyhand.model.user.role.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {

    Optional<Role> findByName(ERole name);
    boolean existsByName(ERole name);

}
