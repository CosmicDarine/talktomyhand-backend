package ch.heigvd.talktomyhand.repository;

import ch.heigvd.talktomyhand.model.image.File;
import ch.heigvd.talktomyhand.model.image.ProfilePicture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface FileRepository extends JpaRepository<File, String> {
    Optional<File> getFileByName(String name);

    @Transactional
    void deleteByName(String name);
}
