package ch.heigvd.talktomyhand.repository;

import ch.heigvd.talktomyhand.model.lesson.Lesson;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface LessonRepository  extends JpaRepository<Lesson, Long> {
    List<Lesson> findAllByDifficulty(Integer difficulty);
    Optional<Lesson> findById(long id);
    Optional<Lesson> findByName(String name);
    List<Lesson> findAll();
    boolean existsByName(String name);
}
