package ch.heigvd.talktomyhand.repository;

import ch.heigvd.talktomyhand.model.image.ProfilePicture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface ProfilePictureRepository extends JpaRepository<ProfilePicture, String> {
    Optional<ProfilePicture> findByUsername(String username);

    @Transactional
    void deleteByUsername(String username);
}
