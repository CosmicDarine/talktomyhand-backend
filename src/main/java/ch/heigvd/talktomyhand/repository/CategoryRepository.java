package ch.heigvd.talktomyhand.repository;

import ch.heigvd.talktomyhand.model.dialect.sign.Category;
import ch.heigvd.talktomyhand.model.dialect.sign.Sign;
import ch.heigvd.talktomyhand.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
    Optional<Category> findByName(String name);
    Optional<Category> findByNameAndUser(String name, User user);
    List<Category> findAll();
    boolean existsByNameAndUser(String name, User user);
    List<Category> findAllByUser(User user);


}
