package ch.heigvd.talktomyhand.repository;

import ch.heigvd.talktomyhand.model.user.auth.TokenBlacklistEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TokenBlacklistRepository extends JpaRepository<TokenBlacklistEntity, Long> {

    public Optional<TokenBlacklistEntity> findByToken(String token);

}
