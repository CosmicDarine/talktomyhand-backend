package ch.heigvd.talktomyhand.repository;

import ch.heigvd.talktomyhand.model.dialect.sign.Sign;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SignRepository extends JpaRepository<Sign, Long> {
    Optional<Sign> findByTraduction(String traduction);
    List<Sign> findByTraductionContainsOrDescriptionContains(String keyword1, String keyword2);
}
