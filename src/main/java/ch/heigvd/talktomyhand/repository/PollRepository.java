package ch.heigvd.talktomyhand.repository;

import ch.heigvd.talktomyhand.model.quiz.Poll;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PollRepository extends JpaRepository<Poll, Integer> {
    Optional<Poll> findById(Integer id);
    //Set<Poll> findByQuestionSignContains(LinkedList<String> signNames);
}
