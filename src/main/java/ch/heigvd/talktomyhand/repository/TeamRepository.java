package ch.heigvd.talktomyhand.repository;

import ch.heigvd.talktomyhand.model.user.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TeamRepository extends JpaRepository<Team, Long> {
    Optional<Team> findByName(String name);
    List<Team> findAll();
    boolean existsByName(String name);
}
