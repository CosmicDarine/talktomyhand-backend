package ch.heigvd.talktomyhand.service;

import ch.heigvd.talktomyhand.config.security.jwt.JwtUtils;
import ch.heigvd.talktomyhand.config.security.model.UserDetailsImpl;
import ch.heigvd.talktomyhand.model.lesson.Lesson;
import ch.heigvd.talktomyhand.model.user.Team;
import ch.heigvd.talktomyhand.model.user.User;
import ch.heigvd.talktomyhand.model.user.auth.TokenBlacklistEntity;
import ch.heigvd.talktomyhand.model.user.role.ERole;
import ch.heigvd.talktomyhand.model.user.role.Role;
import ch.heigvd.talktomyhand.model.user.sex.ESex;
import ch.heigvd.talktomyhand.repository.LessonRepository;
import ch.heigvd.talktomyhand.repository.TeamRepository;
import ch.heigvd.talktomyhand.repository.UserRepository;
import ch.heigvd.talktomyhand.repository.UserSettingsRepository;
import ch.heigvd.talktomyhand.service.exception.*;
import io.jsonwebtoken.ExpiredJwtException;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Here we implement UserDetailsService for Spring Security purposes.
 */
@Slf4j
@Service
public class UserService implements UserDetailsService {
    private static final Logger logger = LoggerFactory.getLogger(UserService.class);
    @Autowired
    private AdminUser adminUser;

    /**
     * Used for Spring Security
     */
    @Autowired
    private AuthenticationManager authenticationManager;

    /**
     * Used for Spring Security
     */
    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private ch.heigvd.talktomyhand.service.RoleService roleService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private LessonRepository lessonRepository;

    @Autowired
    private UserSettingsRepository userSettingsRepository;

    @Autowired
    private ch.heigvd.talktomyhand.service.TokenBlacklistService tokenBlacklistService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * Used by Spring Security
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User not found with username: " + username));
        try {
            return UserDetailsImpl.build(user);
        } catch (Exception e) {
            logger.error("Error with loadByUsername : {}", e.getMessage());
            return null;
        }
    }

    private String encryptPassword(String password) {
        return passwordEncoder.encode(password);
    }

    /**
     * Adds a role (that is valid with the DB) to a User. This is the ONLY
     * ALLOWED WAY to add a role to an user. This does not save the user to DB !
     *
     * @param user
     * @param roleName
     * @return a new user instance with the role added
     */
    public User addRole(User user, ERole roleName) {
        log.trace("Adding role {} to user {}", roleName.toString(), user.getUsername());

        Role role = roleService.getRole(roleName);
        return user.toBuilder().role(role).build();
    }

    /**
     * Removes a role (that is valid with the DB) from a User. This is the
     * ONLY ALLOWED WAY to remove a role from an user. This does not save the user
     * to DB !
     *
     * @param user
     * @param roleName
     * @return a new user instance with the role removed
     */
    public User removeRole(User user, ERole roleName) {
        log.trace("Removing role {} to user {}", roleName.toString(), user.getUsername());

        Role role = roleService.getRole(roleName);
        Set<Role> newRoles = user.getRoles().stream().filter(r -> !r.getName().equals(role.getName()))
                .collect(Collectors.toSet());
        return user.toBuilder().roles(newRoles).build();
    }

    /**
     * Register a User if the parameters are correct.
     *
     * @param user the incomplete user without the role and with non encrypted
     *             password.
     * @throws EntityAlreadyExistException if the username or email is taken
     * @throws InternalErrorException      If something bad happens.
     */
    public void register(User user) throws EntityAlreadyExistException, InternalErrorException {
        if (userRepository.existsByUsername(user.getUsername())) {
            log.trace("Register username already exists {}", user.getUsername());
            throw new EntityAlreadyExistException("Username is already taken !");
        } else if (userRepository.existsByEmail(user.getEmail())) {
            log.trace("Register email already exists {}", user.getEmail());
            throw new EntityAlreadyExistException("Email is already taken !");
        } else if (userRepository.existsById(user.getIdUser())) {
            log.trace("Register id already exists {}", user.getIdUser());
            throw new EntityAlreadyExistException("Id is already taken !");
        } else {
            // Here we have a unique email and username.
            // Check for password requirements here if needed

            try {
                // Encrypt Password
                user = user.withPassword(encryptPassword(user.getPassword()));
                // Add basic and valid USER Sex
                user = addRole(user, ERole.ROLE_USER);
/*
                // Create Image
                try {

                    Image image = new Image();
                    image.setImage("EMPTY");
                    imageRepository.save(image);
                    user.setProfilePic(image);

                } catch (Exception e) {
                    logger.warn("No image");
                }
*/
                int age = LocalDate.now().getYear() -
                        user.getBirthdate().getYear() -
                        (user.getBirthdate().getDayOfYear() > LocalDate.now().getDayOfYear() ? 0 : 1);
                user.setPoints(0);
                user.setLevel(0);
                user.setTeam("");

                Set<Lesson> lessons = new HashSet<>();
                user.setCompletedLessons(lessons);

                // Save the user to DB
                userRepository.save(user);
                log.info("Successful register for {}", user);

            } catch (Exception e) {
                if (userRepository.existsByUsername(user.getUsername())) {
                    log.info("Successful register for {}", user.getUsername());
                } else {

                    e.printStackTrace();
                    log.error("{}", e.getMessage());
                    throw new InternalErrorException(e.getMessage());
                }
            }
        }
    }

    /**
     * Generates a JWT to respond to a login request.
     *
     * @param usernameOrEmail Existing username or Email string.
     * @param password        The password from the user (not encrypted).
     * @return JWT that is used to authenticate the user.
     * @throws WrongCredentialsException If the credentials are wrong or the user
     *                                   doesn't exist.
     * @throws InternalErrorException    If something bad happens.
     */
    public String login(String usernameOrEmail, String password)
            throws WrongCredentialsException, InternalErrorException {

        if (usernameOrEmail == null || usernameOrEmail.isEmpty()) {
            throw new IncompleteBodyException();
        }

        try {
            User user = null;

            try {
                logger.info("Login find by email {}", usernameOrEmail);
                user = userRepository.findByEmail(usernameOrEmail).orElseThrow();
            } catch (NoSuchElementException e) {
                // It's not an email it must be the username
                logger.info("Login find by email {} no such element, trying to find by username.", usernameOrEmail);
                user = userRepository.findByUsername(usernameOrEmail).orElseThrow();
            }

            UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(user.getUsername(),
                    password);
            Authentication authentication = authenticationManager.authenticate(authToken);

            SecurityContextHolder.getContext().setAuthentication(authentication);

            String jwt = jwtUtils.generateJwtToken(authentication);

            logger.info("Successful login by {}", usernameOrEmail);

            return jwt;

        } catch (NoSuchElementException e) {
            log.trace("Login find by username {} no such element. Throwing NoSuchElementException.",
                    usernameOrEmail);
            logger.info("Login attempt by {} - username ou email does not exist.", usernameOrEmail);
            throw new WrongCredentialsException();
        } catch (BadCredentialsException e) {
            log.trace("Login bad password provided for existing user {}", usernameOrEmail);
            logger.info("Login attempt by {} with wrong credentials", usernameOrEmail);
            throw new WrongCredentialsException();
        } catch (Exception e) {
            logger.error("Internal Error, original exception message : {}", e.getMessage());
            throw new InternalErrorException(e.getMessage());
        }
    }

    /**
     * Logs out a user by placing the JWT to the TokenBlacklist.
     * <p>
     * //@param jwtHeader the header containing the Bearer JWT
     *
     * @throws InternalErrorException If something bad happens.
     */
    public void logout() throws InternalErrorException {
        try {

            String jwt = SecurityContextHolder.getContext().getAuthentication().getCredentials().toString();
            log.info("JWT to logout : {}", jwt);
            TokenBlacklistEntity tokenBlacklist = TokenBlacklistEntity.builder()
                    .token(jwt)
                    .expiration(jwtUtils.getExpirationTime(jwt))
                    .build();

            tokenBlacklistService.save(tokenBlacklist);
            log.info("Logout successful for user");
        } catch (ExpiredJwtException e) {
            log.warn("Logout attempt with expired token");
            throw new EntityAlreadyExistException("You are already logged out. The Token is expired.");
        } catch (Exception e) {
            log.error("Internal Error, original exception message : {}", e.getMessage());
            throw new InternalErrorException();
        }
    }

    /**
     * Returns the current user of the request from the DB.
     *
     * @return the current user of the request.
     * @throws EntityDoesNotExistException if something bad happens.
     */
    public User getCurrentUser() throws EntityDoesNotExistException {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Optional<User> optUser = userRepository.findByUsername(username);
        return optUser.orElseThrow(() -> new EntityDoesNotExistException("Current user cannot be retrieved from JWT"));
    }

    /**
     * Creates the main admin user with parameters from application.properties
     */
    public void createMainAdminUser() throws InternalErrorException {
        try {
            if (userRepository.existsByUsername(adminUser.username)) {
                log.info("Existing main admin user, with username: {} password: {}", adminUser.username,
                        adminUser.password);

            } else {
                log.info("Creating main admin user, with username: {} password: {}", adminUser.username,
                        adminUser.password);

                User user = User.builder()
                        .username(adminUser.username)
                        .email(adminUser.email)
                        .password(encryptPassword(adminUser.password))
                        .firstname(adminUser.firstname)
                        .lastname(adminUser.lastname)
                        .birthdate(LocalDate.now())
                        .sex(ESex.FEMALE)
                        .points(0)
                        .level(0)
                        .team("")
                        .build();
/*
                // Create Image
                Image image = new Image();
                image.setImage("IMAGE");
                user.setProfilePic(image);
                imageRepository.save(image);
*/
                user = addRole(user, ERole.ROLE_USER);
                user = addRole(user, ERole.ROLE_ADMIN);
                userRepository.save(user);

            }

        } catch (Exception e) {
            log.error("Internal Error, original exception message : {}", e.getMessage());
            throw new InternalErrorException(e.getMessage());
        }
    }

    /**
     * Update the given user to its current value
     *
     * @param user the new value of the user
     * @return the new version of the user
     */
    public User updateUser(User user) {
        if (user.getIdUser() != getCurrentUser().getIdUser()) {
            throw new MethodNotAllowedException();
        }

        if (!user.getPassword().equals("")) {
            logger.debug("Password is {}", user.getPassword());
            user.setPassword(encryptPassword(user.getPassword()));
        } else {
            logger.debug("No password referenced");
            user.setPassword(getCurrentUser().getPassword());
        }
        //user.setProfilePic(getCurrentUser().getProfilePic());
        user.setRoles(getCurrentUser().getRoles());

        user = userRepository.save(user);
        return user;
    }

    /**
     * Update the given user to its current value
     *
     * @param user the new value of the user
     * @return the new version of the user
     */
    public User updateUserByAdmin(User user) {

        encryptPassword(user.getPassword());
        user = userRepository.save(user);
        return user;
    }

    public void updateUserPoints(int pointsToAdd, long lessonId) {

        User user = userRepository.findByUsername(getCurrentUser().getUsername()).get();
        System.out.println(user);

        try {
            //Si l'user fait partie d'une team, on ajoute également les points gagnés à la team.
                if (user.getTeam() != null && !user.getTeam().isEmpty()) {
                    Team team = teamRepository.findByName(user.getTeam()).get();
                    System.out.println("BEFORE -> TEAM POINTS : " + team.getPoints());
                    team.setPoints(team.getPoints() + pointsToAdd);
                    teamRepository.save(team);
                    System.out.println("AFTER -> TEAM POINTS : " + team.getPoints());
                }
        } catch (Exception e) {
            throw new UnknownEntityException("Problème lors de l'attribution des points à la team");
        }
        try {

            //on ajoute la leçon dans la liste des leçons complétées.
            Lesson lesson = lessonRepository.findById(lessonId).get();
            Set<Lesson> lessonsCompletedByUser = user.getCompletedLessons();
            lessonsCompletedByUser.add(lesson);
            user.setCompletedLessons(lessonsCompletedByUser);

        } catch (Exception e) {
            throw new UnknownEntityException("Problème lors de l'ajout de la leçon aux leçons complétées");
        }

        try {

            //on ajoute les points de la leçon à l'utilisateur
            user.setPoints(getCurrentUser().getPoints() + pointsToAdd);
        } catch (Exception e) {
            throw new UnknownEntityException("Problem lors de l'attribution des points");
        }

        try {
            //l'utilisateur monte d'un niveau
            if (user.getLevel() != null) {
                user.setLevel(getCurrentUser().getLevel() + 1);
            }
            else {
                user.setLevel(1);
            }

        } catch (Exception e) {
            throw new UnknownEntityException("Problem lors de l'attribution de niveau");
        }

        try {

            //on sauvegarde les changements
            userRepository.save(user);
        } catch (Exception e) {
            throw new UnknownEntityException("Problem lors de la sauvegarde de l'utilisateur");
        }

    }

    /**
     * Find a user in repository from the given id
     *
     * @param username the username of the user to return
     * @return the user with the given id
     */
    public User getUserByUsername(String username) {
        Optional<User> o = userRepository.findByUsername(username);
        if (o.isPresent()) {
            return o.get();
        } else {
            throw new UnknownEntityException("This user id doesn't exist !");
        }
    }

    /**
     * Find all users in the repository
     *
     * @return a list of all users in data base
     */
    public Iterable<User> getAllUsers() {
        return userRepository.findAll();
    }

    /**
     * Remove the user with the given id from the data base
     *
     * @param username the username of the user to remove from data base
     */
    @Transactional
    public void deleteUserByUsername(String username) {
        try {
            User user = userRepository.findByUsername(username).get();
            System.out.println(user);
            if (user.getTeam() != null) {
                if (user.getTeam() != "") {
                    Team team = teamRepository.findByName(user.getTeam()).get();
                    System.out.println(team);
                    team.getUsers().remove(user);
                    teamRepository.save(team);
                }
            }

            userRepository.deleteByUsername(username);
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    /**
     * Add a friendship between the two users with the given ids
     * @param idUser the id of the first member of the new friendship
     * @param idFriend the id of the second member of the new friendship
     */

    /**
     * Remove all users in the data base
     */
    public void deleteUsers() {
        try {
            userRepository.deleteAll();
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    /**
     * @return retourne la liste de utilisateurs classés par niveau
     */
    public Iterable<User> getAllUsersByLevel() {
        return userRepository.findAll(Sort.by(Sort.Direction.DESC, "level"));
    }

    /**
     * Private static inner class to inject values from application.properties It is
     * used to create the main admin user.
     */
    @Component
    private static class AdminUser {
        @Value("${talktomyhand.admin.username}")
        private String username;

        @Value("${talktomyhand.admin.email}")
        private String email;

        @Value("${talktomyhand.admin.password}")
        private String password;

        @Value("${talktomyhand.admin.firstname}")
        private String firstname;

        @Value("${talktomyhand.admin.lastname}")
        private String lastname;
    }
}
