package ch.heigvd.talktomyhand.service;

import ch.heigvd.talktomyhand.model.dialect.sign.Sign;
import ch.heigvd.talktomyhand.model.user.User;
import ch.heigvd.talktomyhand.repository.FileRepository;
import ch.heigvd.talktomyhand.repository.SignRepository;
import ch.heigvd.talktomyhand.service.exception.InternalErrorException;
import ch.heigvd.talktomyhand.service.exception.MethodNotAllowedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SignService {

    @Autowired
    private SignRepository signRepository;

    @Autowired
    private FileRepository fileRepository;

    public Sign createOrSaveSign(Sign sign) {
        Optional<Sign> f = signRepository.findByTraduction(sign.getTraduction());
        if (f.isPresent() && sign.getId() != f.get().getId()) {
            throw new MethodNotAllowedException("The sign name must be unique.");
        }
        try {
            signRepository.save(sign);
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
        return sign;
    }

    public Iterable<Sign> getSigns() {
        try {
            return signRepository.findAll();
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    public void deleteSignByTraduction(String traduction) {
        try {
                fileRepository.deleteByName(traduction);

            long id = signRepository.findByTraduction(traduction).get().getId();
            signRepository.deleteById(id);
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    public Sign getSignByTraduction(String sign) {
        try {
            return signRepository.findByTraduction(sign).get();
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    public Iterable<Sign> searchSign(String keyword) {
        try {
            return signRepository.findByTraductionContainsOrDescriptionContains(keyword, keyword);
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }
}
