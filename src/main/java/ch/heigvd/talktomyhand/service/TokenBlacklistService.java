package ch.heigvd.talktomyhand.service;

import ch.heigvd.talktomyhand.model.user.auth.TokenBlacklistEntity;
import ch.heigvd.talktomyhand.repository.TokenBlacklistRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Slf4j
@Service
public class TokenBlacklistService {
    @Autowired
    TokenBlacklistRepository repository;

    public void save(TokenBlacklistEntity tokenBlacklist) {
        try {
            repository.save(tokenBlacklist);
        } catch (DataIntegrityViolationException e) {
            log.warn("The token with id {} is already in the blacklist !", tokenBlacklist.getId());
            // Do nothing
        }
    }

    public void delete(TokenBlacklistEntity tokenBlacklist) {
        repository.delete(tokenBlacklist);
    }

    public Optional<TokenBlacklistEntity> get(String token) {
        return repository.findByToken(token);
    }

    public void deleteExpired() {
        Date now = new Date();
        Iterable<TokenBlacklistEntity> tokens = repository.findAll();
        tokens.forEach(t -> {
            // If the token is expired
            if (t.getExpiration() < now.getTime()) {
                delete(t);
            }
        });
    }
}
