package ch.heigvd.talktomyhand.service.exception;

public class WrongCredentialsException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public WrongCredentialsException() {
        super("Wrong credentials provided !");
    }

    public WrongCredentialsException(String message) {
        super(message);
    }
}
