package ch.heigvd.talktomyhand.service.exception;

public class UnknownEntityException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public UnknownEntityException(String message) {
        super(message);
    }

    public UnknownEntityException() {
        super("This entity doesn't exist !");
    }
}
