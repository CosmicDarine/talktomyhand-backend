package ch.heigvd.talktomyhand.service.exception;

public class IncompleteBodyException extends RuntimeException {
    
    private static final long serialVersionUID = 1L;

    public IncompleteBodyException(String message) {
        super(message);
    }

    public IncompleteBodyException() {
        super("Incomplete body payload was provided ! (Maybe missing element in DTO ...");
    }
}
