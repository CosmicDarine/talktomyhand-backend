package ch.heigvd.talktomyhand.service;

import ch.heigvd.talktomyhand.dto.FileCreationDTO;
import ch.heigvd.talktomyhand.dto.FileDTO;
import ch.heigvd.talktomyhand.dto.ProfilePictureCreationDTO;
import ch.heigvd.talktomyhand.model.image.File;
import ch.heigvd.talktomyhand.model.image.ProfilePicture;
import ch.heigvd.talktomyhand.model.user.User;
import ch.heigvd.talktomyhand.repository.FileRepository;
import ch.heigvd.talktomyhand.repository.ProfilePictureRepository;
import ch.heigvd.talktomyhand.service.exception.EntityDoesNotExistException;
import ch.heigvd.talktomyhand.service.exception.InternalErrorException;
import ch.heigvd.talktomyhand.service.exception.MethodNotAllowedException;
import ch.heigvd.talktomyhand.service.exception.UnknownEntityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Service
public class FileService {

    @Autowired
    private FileRepository fileRepository;

    @Autowired
    private ProfilePictureRepository profilePictureRepository;

    @Autowired
    private UserService userService;

    public File createOrUpdateSignFileForCurrentUser(FileCreationDTO fileCreationDTO) {

        File file;
        if(fileRepository.getFileByName(fileCreationDTO.getName()).isPresent()) {
            file = fileRepository.getFileByName(fileCreationDTO.getName()).get();
        }
        else {
            file = File.builder()
                    .name(fileCreationDTO.getName())
                    .build();
        }

        if(fileCreationDTO.getVideo() != null && fileCreationDTO.getVideo() != "") {
            file.setVideo(fileCreationDTO.getVideo().getBytes());
        }

        if(fileCreationDTO.getGltf() != null && fileCreationDTO.getGltf() != "") {
            file.setGltf(fileCreationDTO.getGltf().getBytes());
        }

        return fileRepository.save(file);
    }

    public File getFile(String name) {
        return fileRepository.getFileByName(name).orElseThrow(() -> new UnknownEntityException("You don't have a file related to this sign for the moment."));
    }


}