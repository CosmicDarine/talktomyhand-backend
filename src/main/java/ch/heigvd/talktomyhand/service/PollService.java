package ch.heigvd.talktomyhand.service;

import ch.heigvd.talktomyhand.model.quiz.Poll;
import ch.heigvd.talktomyhand.repository.PollRepository;
import ch.heigvd.talktomyhand.service.exception.InternalErrorException;
import ch.heigvd.talktomyhand.service.exception.MethodNotAllowedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PollService {

    @Autowired
    private PollRepository pollRepository;

    public Poll createOrSavePoll(Poll poll) {
        if(poll.getId() != null) {
            Optional<Poll> f = pollRepository.findById(poll.getId());
            if (f.isPresent() && poll.getId() != f.get().getId()) {
                throw new MethodNotAllowedException("The poll id must be unique.");
            }
        }
        try {
            Poll p = Poll.builder()
                    .question(poll.getQuestion())
                    .questionSign(poll.getQuestionSign())
                    .propositions(poll.getPropositions())
                    .build();

            pollRepository.save(p);
        } catch (Exception e) {
            throw new InternalErrorException("CREATE OR SAVE POLL : " + e.getMessage());
        }
        return poll;
    }

    public Poll getPollById(Integer id) {
        try {
            return pollRepository.findById(id).get();
        } catch (Exception e) {
            throw new InternalErrorException("GetPollById : " + e.getMessage());
        }
    }

    public Iterable<Poll> getPolls() {
        try {
            return pollRepository.findAll();
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    public void deletePoll(Integer id) {
        try {
            pollRepository.deleteById(id);
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    /*
    public Iterable<Poll> getPollsBySigns(LinkedList<String> signNames) {
        try {
            return pollRepository.findByQuestionSignContains(signNames);
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }*/


}
