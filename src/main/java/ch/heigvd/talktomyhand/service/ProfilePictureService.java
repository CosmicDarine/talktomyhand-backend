package ch.heigvd.talktomyhand.service;

import ch.heigvd.talktomyhand.dto.ProfilePictureCreationDTO;
import ch.heigvd.talktomyhand.model.image.ProfilePicture;
import ch.heigvd.talktomyhand.model.user.User;
import ch.heigvd.talktomyhand.repository.ProfilePictureRepository;
import ch.heigvd.talktomyhand.repository.UserRepository;
import ch.heigvd.talktomyhand.service.exception.EntityDoesNotExistException;
import ch.heigvd.talktomyhand.service.exception.InternalErrorException;
import ch.heigvd.talktomyhand.service.exception.UnknownEntityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Optional;

@Service
public class ProfilePictureService {

    @Autowired
    ProfilePictureRepository profilePictureRepository;

    @Autowired
    UserService userService;

    public void createOrUpdateProfilePictureForCurrentUser(ProfilePictureCreationDTO profilePictureCreationDTO) {
        User user = userService.getCurrentUser();
        ProfilePicture profilePicture = ProfilePicture.builder().image(profilePictureCreationDTO.getImage().getBytes()).username(user.getUsername()).build();

        Optional<ProfilePicture> optionalProfilePicture = profilePictureRepository.findByUsername(user.getUsername());
        if(optionalProfilePicture.isPresent()) {
            profilePicture = optionalProfilePicture.get();
            profilePicture.setImage(optionalProfilePicture.get().getImage());
            profilePictureRepository.deleteByUsername(user.getUsername());
        }

        profilePictureRepository.save(profilePicture);
    }

    public ProfilePicture getProfilePictureForCurrentUser() {
        User user = userService.getCurrentUser();
        Optional<ProfilePicture> profilePicture;

        /*
        if(!profilePictureRepository.findByUsername(user.getUsername()).isPresent())
        {
            try {
                BufferedImage myPicture = ImageIO.read(new File("./logo.png"));
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ImageIO.write(myPicture, "png", baos);
                byte[] bytes = baos.toByteArray();

                ProfilePicture profilePictureTemp = ProfilePicture.builder().image(bytes).username(user.getUsername()).build();
                profilePictureRepository.save(profilePictureTemp);

            } catch (Exception e) {
                throw new InternalErrorException("Problem occured when trying to create default profile picture");
            }
        }*/

        profilePicture = profilePictureRepository.findByUsername(user.getUsername());
        return profilePicture.orElseThrow(() -> new EntityDoesNotExistException("You don't have a profile picture for the moment."));
    }
}
