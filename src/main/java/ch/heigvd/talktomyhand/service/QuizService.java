package ch.heigvd.talktomyhand.service;

import ch.heigvd.talktomyhand.model.dialect.sign.Sign;
import ch.heigvd.talktomyhand.model.quiz.Poll;
import ch.heigvd.talktomyhand.model.quiz.Quiz;
import ch.heigvd.talktomyhand.repository.QuizRepository;
import ch.heigvd.talktomyhand.service.exception.EntityAlreadyExistException;
import ch.heigvd.talktomyhand.service.exception.EntityDoesNotExistException;
import ch.heigvd.talktomyhand.service.exception.InternalErrorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class QuizService {

    @Autowired
    private QuizRepository quizRepository;

    @Autowired
    private SignService signService;

    @Autowired
    private PollService pollService;

    private static final Logger logger = LoggerFactory.getLogger(UserService.class);

    @Transactional
    public Quiz createOrSaveQuiz(String name, List<Integer> pollIds) {
        Quiz quiz;
        Set<Poll> pollsList = new HashSet<>();
        try {
            for(Integer pollId : pollIds) {
                if(pollService.getPollById(pollId) != null) {
                    pollsList.add(pollService.getPollById(pollId));
                }
                else
                {
                    throw new EntityDoesNotExistException("Ce poll n'existe pas.");
                }
            }
            quiz = Quiz.builder().name(name).build();

            if(quizRepository.existsByName(name)) {
                quiz = quizRepository.findByName(name).get();
            }

            quiz.setPolls(pollsList);

            quiz = quizRepository.save(quiz);
            logger.info("Successful creation of {}", quiz);


        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
        return quiz;
    }

    /**
     * Récupère tous les quizzes
     * Commande administrateur
     * @return tous les quizzes
     */
    public List<Quiz> getQuizzes() {
        try {
            logger.info("Successful getQuizzes");
            return quizRepository.findAll();
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    /**
     * Récupère le quiz selon son nom
     * @param name : le nom du quiz
     * @return un quiz
     */
    public Quiz getQuiz(String name) {
        try {
            return quizRepository.findByName(name).get();
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    /**
     * Supprimer un quiz selon son nom.
     * @param name : le nom du quiz
     */
    public void deleteQuiz(String name) {
        try {
            Quiz quiz = quizRepository.findByName(name).get();

            Set<Poll> polls = new HashSet<>();
            quiz.setPolls(polls);
            long id = quizRepository.findByName(name).get().getId();

            quizRepository.deleteById(id);
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    /**
     * Ajoute une liste de polls à un quiz
     * @param pollIds : les signes à ajouter
     * @param quizName : le nom du quiz
     */
    public void addPollsToQuiz(List<Integer> pollIds, String quizName) {
        if(pollIds.isEmpty()) {
            throw new InternalErrorException("La liste de polls spécifiée est vide.");
        }
        try
        {
            Quiz quiz = quizRepository.findByName(quizName).get();
            Set<Poll> pollsList = new HashSet<>();
            for(Integer pollId : pollIds) {
                pollsList.add(pollService.getPollById(pollId));
            }

            quiz.setPolls(pollsList);

            quizRepository.save(quiz);
            logger.info("Polls added successfully to the quiz {}", quizName);
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }
}
