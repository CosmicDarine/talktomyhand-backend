package ch.heigvd.talktomyhand.service;

import ch.heigvd.talktomyhand.dto.SignDTO;
import ch.heigvd.talktomyhand.model.dialect.sign.Category;
import ch.heigvd.talktomyhand.model.dialect.sign.Sign;
import ch.heigvd.talktomyhand.repository.CategoryRepository;
import ch.heigvd.talktomyhand.service.exception.EntityAlreadyExistException;
import ch.heigvd.talktomyhand.service.exception.EntityDoesNotExistException;
import ch.heigvd.talktomyhand.service.exception.InternalErrorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class CategoryService {
    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private SignService signService;

    private static final Logger logger = LoggerFactory.getLogger(UserService.class);

    /**
     * Permet de créer une catégorie avec un nom et une liste de signes.
     * Une catégorie contient toujours des signes, de ce fait, elle ne peut pas être vide.
     * @param name : le nom de la catégorie.
     * @param signTraductions : les signes à ajouter à la catégorie, dont on spécifie uniquement leur nom.
     * @return
     */
    //Transactional permet d'annuler tous les changements qui ont été fait si une erreur survient.
    @Transactional
    public Category createCategory(String name, List<String> signTraductions) {
        if(signTraductions.isEmpty()) {
            throw new InternalErrorException("La liste de signes spécifiée est vide.");
        }
        Category category;
        Set<Sign> signsList = new HashSet<>();
        try {
            for(String sign : signTraductions) {
                if(signService.getSignByTraduction(sign) != null) {
                    signsList.add(signService.getSignByTraduction(sign));
                }
                else
                {
                    throw new EntityDoesNotExistException("Ce signe n'existe pas.");
                }
            }
            category = Category.builder().name(name).user(userService.getCurrentUser()).build();
            if(categoryRepository.existsByNameAndUser(name, userService.getCurrentUser())) {
                throw new EntityAlreadyExistException("Cette catégorie existe déjà.");
            }

            category.setSigns(signsList);

            category = categoryRepository.save(category);
            logger.info("Successful creation of {}", category);


        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
        return category;
    }

    /**
     * Modifie la catégorie selon les paramètres spécifiés.
     * Si la liste de signe est vide, les signes déjà présents dans la catégorie ne seront pas supprimés; ils resteront intactes.
     * Si le nom de la catégorie est le même que le nouveau nom, ledit nom ne changera pas non plus.
     * @param currentName : le nom courant de la catégorie.
     * @param newName ; le nouveau nom.
     * @param signTraductions : les signes spécifiés.
     */
    @Transactional
    public void updateCategory(String currentName, String newName, List<String> signTraductions) {
        //On check que le nom spécifié ne soit pas déjà pris.
        if(categoryRepository.existsByNameAndUser(newName, userService.getCurrentUser())) {
            if(!currentName.equals(newName)) {
                throw new EntityAlreadyExistException("Cette catégorie existe déjà.");
            }
        }
        //On récupère la catégorie
        Optional<Category> category = categoryRepository.findByNameAndUser(currentName, userService.getCurrentUser());

        //On charge la nouvelle liste de signe, si l'user en spécifie.
        try {
            if(!signTraductions.isEmpty()) {
                Set<Sign> signsList = new HashSet<>();
                for(String sign : signTraductions) {
                    if(signService.getSignByTraduction(sign) != null) {
                        signsList.add(signService.getSignByTraduction(sign));
                    }
                    else
                    {
                        throw new EntityDoesNotExistException("Ce signe n'existe pas.");
                    }
                }
                category.get().setSigns(signsList);
            }
            //On change le nom de la catégorie.
            category.get().setName(newName);
            logger.info("Successful creation of {}", category);

        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    /**
     * Récupère toutes les catégories, tout user confondu
     * Commande administrateur
     * @return toutes les catégories, tout user confondu
     */
    public List<Category> getCategories() {
        try {
            logger.info("Successful getCategories");
            return categoryRepository.findAll();
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }


    /**
     * Ajouter plusieurs signes à une catégorie
     * @param signs : la liste de signes à ajouter
     * @param categoryName : le nom de la catégorie où les signes seront ajoutés
     */
    public void addSignsToCategoryForCurrentUser(List<String> signs, String categoryName) {
        if(signs.isEmpty()) {
            throw new InternalErrorException("La liste de signes spécifiée est vide.");
        }
        try {
            //Find la catégorie du repo for current user
            Category category = categoryRepository.findByNameAndUser(categoryName, userService.getCurrentUser()).get();

            //Set<Sign> signsList = new HashSet<>();
            for(String sign : signs) {
                category.getSigns().add(signService.getSignByTraduction(sign));
            }

            //sauvegarder la catégorie
            categoryRepository.save(category);

            logger.info("Sign(s) added successfully to the category {}", categoryName);
        } catch(Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    /**
     * @return toutes les catégories crées par l'utilisateur.
     */
    public List<Category> getAllCategoriesForCurrentUser() {
        try {
            logger.info("Successful getCategories for {}", userService.getCurrentUser().getUsername());
            return categoryRepository.findAllByUser(userService.getCurrentUser());
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    /**
     * Delete the specified category.
     * @param name : the name of the category about to be deleted.
     */
    public void deleteCategory(String name) {
        try {
            long id = categoryRepository.findByName(name).get().getIdCategory();
            categoryRepository.deleteById(id);
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    /*
    public Set<Sign> getAllSignsByCategoryName(String categoryName) {
        try {
            List<SignDTO> signs = new LinkedList<>();

            //On cherche la category par nom et créé par l'utilisateur
            Category category = categoryRepository.findByNameAndUser(categoryName, userService.getCurrentUser()).get();
            return category.getSigns();

        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }*/

    /**
     * @param name : le nom de la catégorie
     * @return la catégorie dont le nom est spécifié.
     */
    public Category getCategory(String name) {
        try {
            return categoryRepository.findByName(name).get();
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }
}
