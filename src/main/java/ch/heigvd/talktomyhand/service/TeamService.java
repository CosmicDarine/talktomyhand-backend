package ch.heigvd.talktomyhand.service;

import ch.heigvd.talktomyhand.model.user.Team;
import ch.heigvd.talktomyhand.model.user.User;
import ch.heigvd.talktomyhand.repository.TeamRepository;
import ch.heigvd.talktomyhand.repository.UserRepository;
import ch.heigvd.talktomyhand.service.exception.EntityAlreadyExistException;
import ch.heigvd.talktomyhand.service.exception.EntityDoesNotExistException;
import ch.heigvd.talktomyhand.service.exception.InternalErrorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;

@Service
public class TeamService {
    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    private static final Logger logger = LoggerFactory.getLogger(UserService.class);

    /**
     * Créé une équipe selon le nom spécifié.
     * @param name : le nom de l'équipe
     * @return l'équipe créée
     */
    @Transactional
    public Team createTeam(String name) {
        Team team;
        try {

            if(name.equals("")) {
                throw new InternalErrorException("Le nom de l'équipe spécifié est vide.");
            }

            if(teamRepository.existsByName(name)) {
                throw new EntityAlreadyExistException("Cette team existe déjà.");
            }

            List<User> users = new LinkedList<>();

            team = Team.builder().name(name).users(users).points(0).build();

            team = teamRepository.save(team);
            logger.info("Successful creation of {}", team);
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
        return team;
    }

    /**
     * @return toutes les équipes ayant été créées.
     */
    public List<Team> getAllTeams() {
        try {
            logger.info("Successful getTeams");
            return teamRepository.findAll();
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    public Team getTeamByName(String name) {
        try {

            if(name.equals("")) {
                throw new InternalErrorException("Le nom de l'équipe spécifié est vide.");
            }

            if(!teamRepository.existsByName(name)) {
                throw new EntityAlreadyExistException("Cette team existe déjà.");
            } else {
                logger.info("Successful get of team {}", name);
                return teamRepository.findByName(name).get();
            }
        }catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }

    }

    /**
     * Supprime une équipe selon le nom renseigné.
     * @param name : le nom de la team.
     */
    public void deleteTeam(String name) {
        try {
            if(name.equals("") || !teamRepository.existsByName(name)) {
                throw new EntityDoesNotExistException("Cette team n'existe pas");
            }

            Team team = teamRepository.findByName(name).get();

            //On commence par réinitialiser l'attribut team de chaque user de la team.
            for(User user : team.getUsers()) {
                user.setTeam("");
            }

            long id = team.getIdTeam();
            teamRepository.deleteById(id);
            logger.info("Successful delete of team {}", name);
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    /**
     * Met à jour le nom de la catégorie.
     * @param name : le nom de la catégorie à modifier
     * @param newName : le nouveau nom donné à la catégorie
     */

    public void updateTeamName(String name, String newName) {
        try {
            if(name.equals("") || !teamRepository.existsByName(name)) {
                throw new EntityDoesNotExistException("Cette team n'existe pas");
            }
            Team team = teamRepository.findByName(name).get();
            team.setName(newName);
            teamRepository.save(team);
            logger.info("Successful modify of team {} in {}", name, newName);
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    /**
     * Inscris ou désinscris un utilisateur à une équipe.
     * @param name : le nom de la team
     * @param userName : le pseudo de l'utilisateur
     */
    public void subscribe(String name, String userName) {
        try {
            if(name.equals("") || !teamRepository.existsByName(name)) {
                throw new EntityDoesNotExistException("Cette team n'existe pas");
            }

            if(userName.equals("") || !userRepository.existsByUsername(userName)) {
                throw new EntityDoesNotExistException("Cet utilisateur n'existe pas");
            }

            //On recherche la team en question.
            Team team = teamRepository.findByName(name).get();
            System.out.println(team);

            //On recherche l'utilisateur en question.
            User user = userRepository.findByUsername(userName).get();
            System.out.println(user);

            //Si l'utilisateur fait partie de la team, on l'enlève, sinon on le rajoute.
            if(team.getUsers().contains(user)) {
                team.getUsers().remove(user);
                user.setTeam(null);
                logger.info("{} successfully unsubscribed from {}", userName, name);
            }
            else {
                team.getUsers().add(user);
                user.setTeam(team.getName());
                logger.info("{} successfully subscribed to {}", userName, name);
            }

            teamRepository.save(team);
            userRepository.save(user);
            System.out.println("AFTEEEEEEEEEEEER\n" + user);


        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

}
