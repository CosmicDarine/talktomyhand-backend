package ch.heigvd.talktomyhand.service;

import ch.heigvd.talktomyhand.model.quiz.Quiz;
import ch.heigvd.talktomyhand.service.exception.EntityAlreadyExistException;
import ch.heigvd.talktomyhand.service.exception.EntityDoesNotExistException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ch.heigvd.talktomyhand.model.dialect.sign.Sign;
import ch.heigvd.talktomyhand.model.lesson.Lesson;
import ch.heigvd.talktomyhand.repository.LessonRepository;
import ch.heigvd.talktomyhand.service.exception.InternalErrorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.CascadeType;
import javax.persistence.OneToOne;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Service
public class LessonService {
    @Autowired
    private LessonRepository lessonRepository;

    @Autowired
    private SignService signService;

    @Autowired
    private QuizService quizService;

    private static final Logger logger = LoggerFactory.getLogger(UserService.class);

    /**
     * Permet de créer ou de modifier une leçon.
     * @param name : le nom de la leçon
     * @param signTraductions : la traduction FR d'un signe.
     * @param quizName : le nom du quizz
     * @return une leçon
     */
    @Transactional
    public Lesson createOrSaveLesson(String name, Integer difficulty, Integer points, List<String> signTraductions, String quizName) {
        Lesson lesson;

        Set<Sign> signsList = new HashSet<>();

        try {
            for(String sign : signTraductions) {
                if(signService.getSignByTraduction(sign) != null) {
                    signsList.add(signService.getSignByTraduction(sign));
                }
                else
                {
                    throw new EntityDoesNotExistException("Ce signe n'existe pas.");
                }
            }

            lesson = Lesson.builder().name(name).difficulty(difficulty).points(points).build();

            if(lessonRepository.existsByName(name)) {
                throw new EntityAlreadyExistException("Cette lesson existe déjà.");
            }

            lesson.setSigns(signsList);
            lesson.setQuiz(quizService.getQuiz(quizName));
            System.out.println("\nMIAOUH-2\n");
            lesson = lessonRepository.save(lesson);
            logger.info("Successful creation of {}", lesson);


        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }

        return lesson;
    }

    /**
     * @return toutes les leçons créées
     */
    public Iterable<Lesson> getLessons() {
        try {
            logger.info("Successful getLessons");
            return lessonRepository.findAll();
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    /**
     * @param name : le nom de la leçon
     * @return la leçon citée
     */
    public Lesson getLessonByName(String name) {
        try {
            return lessonRepository.findByName(name).get();
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    /**
     * @param difficulty : le niveau de difficulté d'une leçon
     * @return les leçon selon son niveau de difficulté
     */
    public List<Lesson> getLessonByDifficulty(Integer difficulty) {
        try {
            return lessonRepository.findAllByDifficulty(difficulty);
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    /**
     * Supprimer un leçon selon son nom.
     * @param name : le nom de la leçon
     */
    public void deleteLesson(String name) {
        try {
            //On enlève les quizzes et signes
            Lesson lesson = lessonRepository.findByName(name).get();
            /*Set<Sign> signs = new HashSet<>();
            Quiz quiz = new Quiz();
            lesson.setQuiz(quiz);
            lesson.setSigns(signs);
            lessonRepository.save(lesson);*/

            //on la supprime
            long id = lesson.getId();
            lessonRepository.deleteById(id);
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    /**
     * Ajouter plusieurs signes à une lesson
     * @param signs : la liste de signes à ajouter
     * @param lessonName : le nom de la lesson où les signes seront ajoutés
     */
    public void addSignsToLesson(List<String> signs, String lessonName) {
        if(signs.isEmpty()) {
            throw new InternalErrorException("La liste de signes spécifiée est vide.");
        }
        try {
            //Find la leçon du repo for current user
            Lesson lesson = lessonRepository.findByName(lessonName).get();

            Set<Sign> signsList = new HashSet<>();
            for(String sign : signs) {
                signsList.add(signService.getSignByTraduction(sign));
            }

            lesson.setSigns(signsList);

            //sauvegarder la catégorie
            lessonRepository.save(lesson);

            logger.info("Signs added successfully to the lesson {}", lessonName);
        } catch(Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    public void addQuizToLesson(String quizName, String lessonName) {
        if(quizName.equals("")) {
            throw new InternalErrorException("Le nom du quiz est vide");
        }

        try {
            Lesson lesson = lessonRepository.findByName(lessonName).get();
            lesson.setQuiz(quizService.getQuiz(quizName));
            lessonRepository.save(lesson);

            logger.info("Quiz {} added successfully to the lesson {}", quizName, lessonName);
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }



}
