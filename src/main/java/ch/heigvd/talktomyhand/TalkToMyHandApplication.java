package ch.heigvd.talktomyhand;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TalkToMyHandApplication {

    public static void main(String[] args) {
        SpringApplication.run(TalkToMyHandApplication.class, args);
    }
}

