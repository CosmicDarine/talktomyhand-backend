package ch.heigvd.talktomyhand.controller.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Home redirection to swagger api documentation
 */

@Controller
@ApiIgnore
public class HomeController {
    @RequestMapping(value = "/")
    public String home() {
        return "redirect:/swagger-ui/";
    }
}
