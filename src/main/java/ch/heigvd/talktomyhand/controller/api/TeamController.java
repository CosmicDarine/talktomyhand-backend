package ch.heigvd.talktomyhand.controller.api;

import ch.heigvd.talktomyhand.api.TeamApi;
import ch.heigvd.talktomyhand.controller.util.ApiHelper;
import ch.heigvd.talktomyhand.dto.*;
import ch.heigvd.talktomyhand.mapper.TeamMapper;
import ch.heigvd.talktomyhand.model.user.Team;
import ch.heigvd.talktomyhand.service.TeamService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import javax.validation.Valid;
import java.util.LinkedList;
import java.util.List;

@Api(tags="Team", description = "All endpoints used to manipulate teams.")
@Controller
public class TeamController implements TeamApi {
    @Autowired
    private TeamMapper teamMapper;

    @Autowired
    private TeamService teamService;

    /**
     * @param teamCreationDTO  (optional)
     * @return un message expliquant si l'équipe a bien été créée.
     */
    @Override
    public ResponseEntity<ApiMessageDTO> createTeam(@Valid TeamCreationDTO teamCreationDTO) {
        teamService.createTeam(teamCreationDTO.getName());
        return ResponseEntity.ok(ApiHelper.ok("Team created successfully."));
    }

    /**
     * @return toutes les équipes créées.
     */
    @Override
    public ResponseEntity<List<TeamDTO>> getTeams() {
        List<TeamDTO> teams = new LinkedList<>();
        for(Team team : teamService.getAllTeams()) {
            teams.add(teamMapper.toDto(team));
        }

        return ResponseEntity.ok(teams);
    }

    /**
     * @param name  (required) : le nom de la team
     * @return La team
     */
    @Override
    public ResponseEntity<TeamDTO> getTeam(String name) {
        return ResponseEntity.ok(teamMapper.toDto(teamService.getTeamByName(name)));
    }

    /**
     * Supprime une équipe selon le nom renseigné.
     * @param name  (required) : nom de la team
     * @return : un message expliquant si l'équipe a bien été supprimée.
     */
    @Override
    public ResponseEntity<ApiMessageDTO> deleteTeamByName(@Valid String name) {
        teamService.deleteTeam(name);
        return ResponseEntity.ok(ApiHelper.ok("Team deleted successfully."));
    }

    @Override
    public ResponseEntity<ApiMessageDTO> updateTeamName(@Valid TeamNameModificationDTO teamNameModificationDTO) {
        teamService.updateTeamName(teamNameModificationDTO.getName(), teamNameModificationDTO.getNewName());
        return ResponseEntity.ok(ApiHelper.ok("Team name updated successfully."));
    }

    @Override
    public ResponseEntity<ApiMessageDTO> subscribeToTeam(@Valid TeamSubscriptionDTO teamSubscriptionDTO) {
        teamService.subscribe(teamSubscriptionDTO.getTeamName(), teamSubscriptionDTO.getUser());
        return ResponseEntity.ok(ApiHelper.ok("(Un)Subscription successful."));
    }
}
