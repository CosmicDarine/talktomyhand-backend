package ch.heigvd.talktomyhand.controller.api;

import ch.heigvd.talktomyhand.api.LessonApi;
import ch.heigvd.talktomyhand.controller.util.ApiHelper;
import ch.heigvd.talktomyhand.dto.*;
import ch.heigvd.talktomyhand.mapper.LessonMapper;
import ch.heigvd.talktomyhand.model.dialect.sign.Category;
import ch.heigvd.talktomyhand.model.lesson.Lesson;
import ch.heigvd.talktomyhand.service.LessonService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import javax.validation.Valid;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Api(tags="Lesson", description = "All endpoints used to add, modify and remove lessons in the app.")
@Controller
public class LessonController implements LessonApi {
    @Autowired
    private LessonMapper lessonMapper;

    @Autowired
    private LessonService lessonService;

    @Override
    public ResponseEntity<LessonDTO> createLesson(@Valid LessonCreationDTO lessonCreationDTO) {
        return ResponseEntity.ok(lessonMapper.toDto(lessonService.createOrSaveLesson(
                lessonCreationDTO.getName(),
                lessonCreationDTO.getDifficulty(),
                lessonCreationDTO.getPoints(),
                lessonCreationDTO.getSigns().stream().map(signDTO -> signDTO.getTraduction()).collect(Collectors.toList()),
                lessonCreationDTO.getQuiz().getName())));
    }


    @Override
    public ResponseEntity<LessonDTO> updateLesson(String name, @Valid LessonModificationDTO lessonModificationDTO) {

        return ResponseEntity.ok(lessonMapper.toDto(lessonService.createOrSaveLesson(
                name,
                lessonModificationDTO.getDifficulty(),
                lessonModificationDTO.getPoints(),
                lessonModificationDTO.getSigns().stream().map(signDTO -> signDTO.getTraduction()).collect(Collectors.toList()),
                lessonModificationDTO.getQuiz().getName())));
    }


    @Override
    public ResponseEntity<LessonDTO> getLesson(String name) {
        return ResponseEntity.ok(lessonMapper.toDto(lessonService.getLessonByName(name)));
    }

    @Override
    public ResponseEntity<List<LessonDTO>> getLessons() {
        List<LessonDTO> lessons = new LinkedList<>();
        for(Lesson lesson : lessonService.getLessons()) {
            lessons.add(lessonMapper.toDto(lesson));
        }
        return ResponseEntity.ok(lessons);
    }

    @Override
    public ResponseEntity<ApiMessageDTO> deleteLesson(String name) {
        lessonService.deleteLesson(name);
        return ResponseEntity.ok(ApiHelper.ok("Lesson deleted successfully."));
    }

    @Override
    public ResponseEntity<ApiMessageDTO> addSignToLesson(LessonAddSignDTO lessonAddSignDTO) {
        lessonService.addSignsToLesson(lessonAddSignDTO.getSigns(), lessonAddSignDTO.getName());
        return ResponseEntity.ok(ApiHelper.ok("Signs added successfully to the lesson."));
    }

    @Override
    public ResponseEntity<ApiMessageDTO> addQuizToLesson(LessonAddQuizDTO lessonAddQuizDTO) {
        lessonService.addQuizToLesson(lessonAddQuizDTO.getQuiz(), lessonAddQuizDTO.getName());
        return ResponseEntity.ok(ApiHelper.ok("Quiz added successfully to the lesson."));
    }

}
