package ch.heigvd.talktomyhand.controller.api;

import ch.heigvd.talktomyhand.api.CategoryApi;
import ch.heigvd.talktomyhand.controller.util.ApiHelper;
import ch.heigvd.talktomyhand.dto.*;
import ch.heigvd.talktomyhand.mapper.CategoryMapper;
import ch.heigvd.talktomyhand.model.dialect.sign.Category;
import ch.heigvd.talktomyhand.service.CategoryService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import javax.validation.Valid;
import java.util.LinkedList;
import java.util.List;

@Api(tags="Category", description = "All endpoints used to manipulate categories.")
@Controller
public class CategoryController implements CategoryApi {

    @Autowired
    private CategoryMapper categoryMapper;

    @Autowired
    private CategoryService categoryService;

    @Override
    public ResponseEntity<CategoryDTO> createCategory(@Valid CategoryCreationDTO categoryCreationDTO) {
        return ResponseEntity.ok(categoryMapper.toDto(categoryService.createCategory(categoryCreationDTO.getName(), categoryCreationDTO.getSigns())));
    }

    @Override
    public ResponseEntity<ApiMessageDTO> updateCategory(@Valid CategoryModificationDTO categoryModificationDTO) {
        categoryService.updateCategory(categoryModificationDTO.getName(), categoryModificationDTO.getNewName(),categoryModificationDTO.getSigns());
        return ResponseEntity.ok(ApiHelper.ok("Category updated successfully."));
    }

    @Override
    public ResponseEntity<List<CategoryDTO>> getCategories() {
        List<CategoryDTO> categories = new LinkedList<>();
        for(Category c : categoryService.getAllCategoriesForCurrentUser()) {
            categories.add(categoryMapper.toDto(c));
        }
        return ResponseEntity.ok(categories);
    }

    @Override
    public ResponseEntity<CategoryDTO> getCategory(String name) {
        return ResponseEntity.ok(categoryMapper.toDto(categoryService.getCategory(name)));
    }


    @Override
    public ResponseEntity<ApiMessageDTO> deleteCategoryByName(String name) {
        categoryService.deleteCategory(name);
        return ResponseEntity.ok(ApiHelper.ok("Category deleted successfully."));
    }

    @Override
    public ResponseEntity<ApiMessageDTO> addSignToCategory(CategoryAddSignDTO categoryAddSignDTO) {
        categoryService.addSignsToCategoryForCurrentUser(categoryAddSignDTO.getSigns(), categoryAddSignDTO.getName());
        return ResponseEntity.ok(ApiHelper.ok("Signs added successfully to the category."));
    }

}
