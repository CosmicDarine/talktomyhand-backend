package ch.heigvd.talktomyhand.controller.api;



import ch.heigvd.talktomyhand.api.ProfilePictureApi;
import ch.heigvd.talktomyhand.controller.util.ApiHelper;
import ch.heigvd.talktomyhand.dto.ApiMessageDTO;
import ch.heigvd.talktomyhand.dto.ProfilePictureCreationDTO;
import ch.heigvd.talktomyhand.dto.ProfilePictureDTO;
import ch.heigvd.talktomyhand.model.image.ProfilePicture;
import ch.heigvd.talktomyhand.service.ProfilePictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import javax.validation.Valid;

@Controller
public class ProfilePictureController implements ProfilePictureApi {
    @Autowired
    ProfilePictureService profilePictureService;

    @Override
    public ResponseEntity<ApiMessageDTO> uploadProfilePicture(@Valid ProfilePictureCreationDTO profilePictureCreationDTO) {
        //TODO adapt with Mapper
        profilePictureService.createOrUpdateProfilePictureForCurrentUser(profilePictureCreationDTO);
        return ResponseEntity.ok(ApiHelper.ok("Profile picture updated."));
    }

    @Override
    public ResponseEntity<ProfilePictureDTO> getProfilePicture(String username) {
        ProfilePicture profilePicture = profilePictureService.getProfilePictureForCurrentUser();
        ProfilePictureDTO profilePictureDTO = new ProfilePictureDTO();
        profilePictureDTO.setId(profilePicture.getId());
        profilePictureDTO.setUsername(username);
        profilePictureDTO.setImage(new String(profilePicture.getImage()));
        return ResponseEntity.ok(profilePictureDTO);
    }
}
