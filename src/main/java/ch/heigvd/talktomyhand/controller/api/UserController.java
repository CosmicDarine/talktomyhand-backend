package ch.heigvd.talktomyhand.controller.api;

import ch.heigvd.talktomyhand.api.UserApi;
import ch.heigvd.talktomyhand.controller.util.ApiHelper;
import ch.heigvd.talktomyhand.dto.*;
import ch.heigvd.talktomyhand.mapper.UserMapper;
import ch.heigvd.talktomyhand.model.user.User;
import ch.heigvd.talktomyhand.service.UserService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import javax.validation.Valid;
import java.util.LinkedList;
import java.util.List;


@Api(tags="User", description = "All endpoints used for user modification.")
@Controller
public class UserController implements UserApi {

    @Autowired
    private UserService userService;

    @Autowired
    private UserMapper userMapper;

    @Override
    public ResponseEntity<UserDTO> getUserByUsername(String username) {
        return ResponseEntity.ok(userMapper.toDto(userService.getUserByUsername(username)));
    }

    @Override
    public ResponseEntity<List<UserDTO>> getUsers() {
        Iterable<User> users = userService.getAllUsers();
        List<User> u = new LinkedList<>();
        users.forEach(u::add);
        return ResponseEntity.ok(getUserDTOS(u));
    }

    @Override
    public ResponseEntity<List<UserDTO>> getUsersMinimal() {
        Iterable<User> users = userService.getAllUsers();
        List<User> u = new LinkedList<>();
        users.forEach(u::add);
        return ResponseEntity.ok(getUserDTOS(u));
    }

    @Override
    public ResponseEntity<UserDTO> updateUser(String username, @Valid UserModificationDTO userModificationDTO) {
        return ResponseEntity.ok(userMapper.toDto(userService.updateUser(userMapper.toModelFromModification(userModificationDTO))));
    }

    @Override
    public ResponseEntity<ApiMessageDTO> updateUserPoints(@Valid UserAddPointsForLessonDTO userAddPointsForLessonDTO) {
        userService.updateUserPoints(userAddPointsForLessonDTO.getPointsToAdd(), userAddPointsForLessonDTO.getLessonId());
        return ResponseEntity.ok(ApiHelper.ok("User points modified successfully"));
    }

    @Override
    public ResponseEntity<ApiMessageDTO> deleteUser(String username) {
        userService.deleteUserByUsername(username);
        return ResponseEntity.ok(ApiHelper.ok("User deleted successfully."));
    }

    @Override
    public ResponseEntity<ApiMessageDTO> deleteUsers() {
        userService.deleteUsers();
        return ResponseEntity.ok(ApiHelper.ok("All users deleted successfully."));
    }

    @Override
    public ResponseEntity<ApiMessageDTO> updateUserByAdmin(String username, @Valid UserDTO userDTO) {
        userService.updateUserByAdmin(userMapper.toModel(userDTO));
        return ResponseEntity.ok(ApiHelper.ok("User modified by admin successfully"));
    }

    private List<UserDTO> getUserDTOS(List<User> userEntities) {
        List<UserDTO> dtos = new LinkedList<>();
        userEntities.forEach(m -> {
            dtos.add(userMapper.toDto(m));
        });
        return dtos;
    }

}
