package ch.heigvd.talktomyhand.controller.api;

import ch.heigvd.talktomyhand.api.PollApi;
import ch.heigvd.talktomyhand.controller.util.ApiHelper;
import ch.heigvd.talktomyhand.dto.ApiMessageDTO;
import ch.heigvd.talktomyhand.dto.PollCreationDTO;
import ch.heigvd.talktomyhand.dto.PollDTO;

import ch.heigvd.talktomyhand.dto.PollModificationDTO;
import ch.heigvd.talktomyhand.mapper.PollMapper;
import ch.heigvd.talktomyhand.model.quiz.Poll;
import ch.heigvd.talktomyhand.service.PollService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import javax.validation.Valid;
import java.util.LinkedList;
import java.util.List;

@Api(tags="Poll", description = "All endpoints used to add, modify and remove polls in the app.")
@Controller
public class PollController implements PollApi {
    @Autowired
    private PollMapper pollMapper;

    @Autowired
    private PollService pollService;

    @Override
    public ResponseEntity<PollDTO> createPoll(@Valid PollCreationDTO pollCreationDTO) {
        return ResponseEntity.ok(pollMapper.toDto(pollService.createOrSavePoll(pollMapper.toModelFromCreation(pollCreationDTO))));
    }

    @Override
    public ResponseEntity<PollDTO> getPollById(Integer id) {
        return ResponseEntity.ok(pollMapper.toDto(pollService.getPollById(id)));
    }

    @Override
    public ResponseEntity<List<PollDTO>> getPolls() {
        Iterable<Poll> pollList = pollService.getPolls();
        List<PollDTO> pollDTOS = new LinkedList<>();
        pollList.forEach(poll -> {
            pollDTOS.add(pollMapper.toDto(poll));
        });
        return ResponseEntity.ok(pollDTOS);
    }

    @Override
    public ResponseEntity<ApiMessageDTO> deletePollById(Integer id) {
        pollService.deletePoll(id);
        return ResponseEntity.ok(ApiHelper.ok("Poll deleted successfully."));
    }

    @Override
    public ResponseEntity<PollDTO> updatePoll(@Valid PollModificationDTO pollModificationDTO) {
        return ResponseEntity.ok(pollMapper.toDto(pollService.createOrSavePoll(pollMapper.toModelFromModification(pollModificationDTO))));
    }
}
