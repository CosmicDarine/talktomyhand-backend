package ch.heigvd.talktomyhand.controller.api;

import ch.heigvd.talktomyhand.api.AuthenticationApi;
import ch.heigvd.talktomyhand.controller.util.ApiHelper;
import ch.heigvd.talktomyhand.dto.ApiMessageDTO;
import ch.heigvd.talktomyhand.dto.LoginRequestDTO;
import ch.heigvd.talktomyhand.dto.LoginSuccessDTO;
import ch.heigvd.talktomyhand.dto.RegisterDTO;
import ch.heigvd.talktomyhand.mapper.UserMapper;
import ch.heigvd.talktomyhand.model.user.User;
import ch.heigvd.talktomyhand.service.UserService;
import ch.heigvd.talktomyhand.service.exception.IncompleteBodyException;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Api(tags = "Authentication", description = "All endpoints used for authentication and registration.")
@RestController
@Slf4j
public class AuthController implements AuthenticationApi {

    @Autowired
    private UserService userService;

    @Autowired
    private UserMapper userMapper;

    @Override
    public ResponseEntity<ApiMessageDTO> register(@Valid RegisterDTO registerDTO) {
        try {
            User user = userMapper.toModel(registerDTO);
            userService.register(user);
        } catch (NullPointerException e) {
            throw new IncompleteBodyException();
        }
        return ResponseEntity.ok(ApiHelper.created("Register successful"));
    }

    @Override
    public ResponseEntity<LoginSuccessDTO> login(@Valid LoginRequestDTO loginRequestDTO) {
        String jwt = userService.login(loginRequestDTO.getUsernameOrEmail(), loginRequestDTO.getPassword());
        User user = userService.getCurrentUser();

        LoginSuccessDTO loginSuccessDTO = userMapper.toLoginSuccessDTO(user, jwt);

        return ResponseEntity.ok(loginSuccessDTO);
    }

    @Override
    public ResponseEntity<ApiMessageDTO> logout() {
        userService.logout();
        return ResponseEntity.ok(ApiHelper.ok("Logout successful"));
    }
}
