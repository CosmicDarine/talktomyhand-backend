package ch.heigvd.talktomyhand.controller.api;

import ch.heigvd.talktomyhand.api.DialectApi;
import ch.heigvd.talktomyhand.controller.util.ApiHelper;
import ch.heigvd.talktomyhand.dto.ApiMessageDTO;
import ch.heigvd.talktomyhand.dto.SignCreationDTO;
import ch.heigvd.talktomyhand.dto.SignDTO;
import ch.heigvd.talktomyhand.dto.SignModificationDTO;
import ch.heigvd.talktomyhand.mapper.SignMapper;

import ch.heigvd.talktomyhand.model.dialect.sign.Sign;
import ch.heigvd.talktomyhand.service.FileService;
import ch.heigvd.talktomyhand.service.SignService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import javax.validation.Valid;
import java.util.LinkedList;
import java.util.List;

@Api(
    tags="Dialect",
    description = "All endpoints used to add, modify and remove elements linked to the language part of the app.")
@Controller
public class SignController implements DialectApi {
    @Autowired
    private SignMapper signMapper;

    @Autowired
    private SignService signService;

    @Override
    public ResponseEntity<SignDTO> createSign(@Valid SignCreationDTO signCreationDTO) {
        return ResponseEntity.ok(signMapper.toDto(signService.createOrSaveSign(signMapper.toModelFromCreation(signCreationDTO))));
    }

    @Override
    public ResponseEntity<List<SignDTO>> getSigns() {
        Iterable<Sign> signList = signService.getSigns();
        List<SignDTO> signDTOList = new LinkedList<>();
        signList.forEach(sign -> {
            signDTOList.add(signMapper.toDto(sign));
        });
        return ResponseEntity.ok(signDTOList);
    }

    @Override
    public ResponseEntity<SignDTO> getSignByTraduction(String traduction) {
        return ResponseEntity.ok(signMapper.toDto(signService.getSignByTraduction(traduction)));
    }

    @Override
    public ResponseEntity<SignDTO> updateSign(@Valid SignModificationDTO signModificationDTO) {
        return ResponseEntity.ok(signMapper.toDto(signService.createOrSaveSign(signMapper.toModelFromModification(signModificationDTO))));
    }

    @Override
    public ResponseEntity<ApiMessageDTO> deleteSignByTraduction(String traduction) {
        signService.deleteSignByTraduction(traduction);
        return ResponseEntity.ok(ApiHelper.ok("Sign deleted successfully."));
    }

    @Override
    public ResponseEntity<List<SignDTO>> searchSign(String keyword) {
        Iterable<Sign> signList = signService.searchSign(keyword);
        List<SignDTO> signDTOList = new LinkedList<>();
        signList.forEach(sign -> {
            signDTOList.add(signMapper.toDto(sign));
        });
        return ResponseEntity.ok(signDTOList);
    }

}

