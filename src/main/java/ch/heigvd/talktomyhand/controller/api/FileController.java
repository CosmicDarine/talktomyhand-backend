package ch.heigvd.talktomyhand.controller.api;

import ch.heigvd.talktomyhand.api.FileApi;
import ch.heigvd.talktomyhand.controller.util.ApiHelper;
import ch.heigvd.talktomyhand.dto.*;
import ch.heigvd.talktomyhand.mapper.FileMapper;
import ch.heigvd.talktomyhand.model.image.File;
import ch.heigvd.talktomyhand.model.image.ProfilePicture;
import ch.heigvd.talktomyhand.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

@Controller
public class FileController implements FileApi {

    @Autowired
    private FileService fileService;

    @Autowired
    private FileMapper fileMapper;

    @Override
    public ResponseEntity<ApiMessageDTO> uploadSignVideo(@Valid FileCreationDTO fileCreationDTO) {
        //TODO adapt with Mapper
        fileService.createOrUpdateSignFileForCurrentUser(fileCreationDTO);
        return ResponseEntity.ok(ApiHelper.ok("Sign files updated."));
    }

    @Override
    public ResponseEntity<FileDTO> getSignVideo(String signName) {
        File file = fileService.getFile(signName);
        FileDTO fileDTO = new FileDTO();
        fileDTO.setName(signName);
        fileDTO.setVideo(new String(file.getVideo()));
        fileDTO.setGltf(new String(file.getGltf()));
        return ResponseEntity.ok(fileDTO);
    }

}