package ch.heigvd.talktomyhand.controller.api;

import ch.heigvd.talktomyhand.api.QuizApi;
import ch.heigvd.talktomyhand.controller.util.ApiHelper;
import ch.heigvd.talktomyhand.dto.*;
import ch.heigvd.talktomyhand.mapper.QuizMapper;
import ch.heigvd.talktomyhand.model.dialect.sign.Category;
import ch.heigvd.talktomyhand.model.quiz.Quiz;
import ch.heigvd.talktomyhand.service.QuizService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import javax.validation.Valid;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Api(tags="Quiz", description = "All endpoints used to add, modify and remove quizzes in the app.")
@Controller
public class QuizController implements QuizApi {
    @Autowired
    private QuizMapper quizMapper;

    @Autowired
    private QuizService quizService;

    @Override
    public ResponseEntity<QuizDTO> createQuiz(@Valid QuizCreationDTO quizCreationDTO) {
        return ResponseEntity.ok(quizMapper.toDto(quizService.createOrSaveQuiz(quizCreationDTO.getName(), quizCreationDTO.getPolls().stream().map(pollDTO -> pollDTO.getId()).collect(Collectors.toList()))));
    }

    @Override
    public ResponseEntity<QuizDTO> updateQuiz(String name, @Valid QuizModificationDTO quizModificationDTO) {
        return ResponseEntity.ok(quizMapper.toDto(quizService.createOrSaveQuiz(name, quizModificationDTO.getPolls().stream().map(pollDTO -> pollDTO.getId()).collect(Collectors.toList()))));
    }

    @Override
    public ResponseEntity<QuizDTO> getQuiz(String name) {
        return ResponseEntity.ok(quizMapper.toDto(quizService.getQuiz(name)));
    }

    @Override
    public ResponseEntity<List<QuizDTO>> getQuizzes() {
        List<QuizDTO> quizzes = new LinkedList<>();
        for(Quiz quiz : quizService.getQuizzes()) {
            quizzes.add(quizMapper.toDto(quiz));
        }
        return ResponseEntity.ok(quizzes);
    }

    @Override
    public ResponseEntity<ApiMessageDTO> deleteQuizByName(String name) {
        quizService.deleteQuiz(name);
        return ResponseEntity.ok(ApiHelper.ok("Quiz deleted successfully."));
    }

    @Override
    public ResponseEntity<ApiMessageDTO> addPollToQuiz(QuizAddPollDTO quizAddPollDTO) {
        quizService.addPollsToQuiz(quizAddPollDTO.getPolls(), quizAddPollDTO.getName());
        return ResponseEntity.ok(ApiHelper.ok("Signs added successfully to the quiz."));
    }

}
