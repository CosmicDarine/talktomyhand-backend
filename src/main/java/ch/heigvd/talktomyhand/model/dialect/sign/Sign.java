package ch.heigvd.talktomyhand.model.dialect.sign;

import lombok.*;
import org.springframework.lang.NonNull;

import javax.persistence.*;

@Data
@Getter
@Setter
@Builder(toBuilder = true)
@With
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Sign")
public class Sign {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true)
    @NonNull
    private String traduction;

    @NonNull
    private String description;

    @NonNull
    private EDifficulty difficulty;
}

