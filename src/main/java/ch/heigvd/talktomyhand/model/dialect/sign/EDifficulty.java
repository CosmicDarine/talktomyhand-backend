package ch.heigvd.talktomyhand.model.dialect.sign;

import ch.heigvd.talktomyhand.service.exception.InternalErrorException;

public enum EDifficulty {
    VERY_EASY,
    EASY,
    MEDIUM,
    HARD,
    VERY_HARD;

    public static EDifficulty getDifficulty(String difficulty) {
        if (difficulty.equals(VERY_EASY.name())) {
            return VERY_EASY;
        } else if (difficulty.equals(EASY.name())) {
            return EASY;
        }
        else if (difficulty.equals(EASY.name())) {
            return EASY;
        }
        else if (difficulty.equals(MEDIUM.name())) {
            return MEDIUM;
        }
        else if (difficulty.equals(HARD.name())) {
            return HARD;
        }
        else if (difficulty.equals(VERY_HARD.name())) {
            return VERY_HARD;
        }
        throw new InternalErrorException("This difficulty does not exist");
    }
}