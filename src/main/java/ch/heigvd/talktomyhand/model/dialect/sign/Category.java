package ch.heigvd.talktomyhand.model.dialect.sign;

import ch.heigvd.talktomyhand.model.user.User;
import lombok.*;
import lombok.experimental.Delegate;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.util.Set;

@With
@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Entity(name = "Category")
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idCategory;

    @NonNull
    private String name;

    @ManyToOne
    private User user;

    @Delegate
    @ManyToMany
    private Set<Sign> signs;
}
