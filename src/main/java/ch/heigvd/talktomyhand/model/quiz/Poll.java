package ch.heigvd.talktomyhand.model.quiz;

import lombok.*;
import org.springframework.lang.NonNull;

import javax.persistence.*;

@Data
@Getter
@Setter
@Builder(toBuilder = true)
@With
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Poll")
public class Poll {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NonNull
    private String question;

    @NonNull
    private String questionSign;

    @NonNull
    private String propositions;
}
