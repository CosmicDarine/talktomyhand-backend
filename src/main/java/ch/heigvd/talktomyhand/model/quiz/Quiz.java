package ch.heigvd.talktomyhand.model.quiz;

import ch.heigvd.talktomyhand.model.dialect.sign.Sign;
import lombok.*;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.util.Set;

@Data
@Getter
@Setter
@Builder(toBuilder = true)
@With
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Quiz")
public class Quiz {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NonNull
    @Column(unique = true)
    private String name;

    @ManyToMany
    private Set<Sign> signs;

    @ManyToMany
    private Set<Poll> polls;

}
