package ch.heigvd.talktomyhand.model.lesson;

import                                     ch.heigvd.talktomyhand.model.dialect.sign.Sign;
import ch.heigvd.talktomyhand.model.quiz.Quiz;
import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Data
@Getter
@Setter
@Builder(toBuilder = true)
@With
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Entity(name = "Lesson")
public class Lesson {
    @Id
    @NonNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NonNull
    @Column(unique = true)
    private String name;

    private Integer difficulty;

    private Integer points;

    //@Delegate
    @ManyToMany
    private Set<Sign> signs;

    @ManyToOne
    private Quiz quiz;

}
