package ch.heigvd.talktomyhand.model.image;

        import lombok.*;

        import javax.persistence.*;

@Data
@Getter
@Setter
@Builder(toBuilder = true)
@With
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "ProfilePicture")
public class ProfilePicture {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String username;

    @Lob
    private byte[] image;
}
