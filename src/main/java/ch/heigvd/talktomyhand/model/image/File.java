package ch.heigvd.talktomyhand.model.image;

import lombok.*;

import javax.persistence.*;

@Data
@Getter
@Setter
@Builder(toBuilder = true)
@With
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "File")
public class File {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;

    @Lob
    private byte[] video;

    @Lob
    private byte[] gltf;


}
