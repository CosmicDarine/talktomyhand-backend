package ch.heigvd.talktomyhand.model.user.sex;

import ch.heigvd.talktomyhand.service.exception.InternalErrorException;

public enum ESex {
	FEMALE,
    MALE,
	UNDEFINED;

	public static ESex getSex(String sex) {
	    if (sex.equals(MALE.name())) {
	        return MALE;
        } else if (sex.equals(FEMALE.name())) {
	        return FEMALE;
        } else if (sex.equals(UNDEFINED.name())) {
	    	return UNDEFINED;
		}
	    throw new InternalErrorException("This sex does not exist");
    }
}
