package ch.heigvd.talktomyhand.model.user;

import lombok.*;

import javax.persistence.*;

@Data
@Getter
@Setter
@Builder(toBuilder = true)
@With
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Entity(name = "UserSettings")
public class UserSettings {
    @Id
    @NonNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idUserSettings;

    @NonNull
    @OneToOne(fetch = FetchType.EAGER)
    private User user;

    private Boolean allowTutorial;

    private Boolean allowCategories;

    private Boolean allowTeams;

    private Boolean allowLessons;
}
