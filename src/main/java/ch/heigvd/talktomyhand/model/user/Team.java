package ch.heigvd.talktomyhand.model.user;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Data
@Getter
@Setter
@Builder(toBuilder = true)
@With
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Entity(name = "Team")
public class Team {
    @Id
    @NonNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idTeam;

    @NonNull
    @Column(unique = true)
    private String name;

    private Integer points;

    @ManyToMany(fetch = FetchType.EAGER)
    @Singular
    private Set<User> users;
}