package ch.heigvd.talktomyhand.model.user;
import ch.heigvd.talktomyhand.model.lesson.Lesson;
import ch.heigvd.talktomyhand.model.user.role.Role;
import ch.heigvd.talktomyhand.model.user.sex.ESex;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Data
@Getter
@Setter
@Builder(toBuilder = true)
@With
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Entity(name = "User")
public class User {
    @Id
    @NonNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idUser;

    @Column(unique = true)
    private String username;

    private String firstname;

    private String lastname;

    @JsonFormat(pattern = "dd-mm-yyyy")
    private LocalDate birthdate;

    @Column(unique = true)
    private String email;

    private String password;

    private ESex sex;

    @ManyToMany(fetch = FetchType.EAGER)
    @Singular
    private Set<Role> roles;

    private Integer points;

    private Integer level;

    private String team;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Lesson> completedLessons;

    /**
     * IMAGE
     */

    //@ManyToOne(fetch = FetchType.LAZY/*,
    //            cascade = CascadeType.ALL*/)
    // private Image profilePic;

}
