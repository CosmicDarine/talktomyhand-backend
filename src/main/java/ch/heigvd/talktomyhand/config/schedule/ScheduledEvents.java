package ch.heigvd.talktomyhand.config.schedule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import javax.annotation.PostConstruct;

import ch.heigvd.talktomyhand.service.RoleService;
import ch.heigvd.talktomyhand.service.TokenBlacklistService;
import ch.heigvd.talktomyhand.service.UserService;

@Configuration
@EnableScheduling
public class ScheduledEvents {

    @Autowired
    private TokenBlacklistService tokenBlacklistService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private UserService userService;

    private static final Logger logger = LoggerFactory.getLogger(UserService.class);
    /**
     * Delete the black listed tokens
     */
    @Scheduled(fixedDelayString = "${talktomyhand.schedule.TokenBlacklistClean}")
    public void deleteBlackListedTokens() {
        tokenBlacklistService.deleteExpired();
        logger.info("Delete the black listed tokens.");
    }

    /**
     * Methods to run after application starts up
     */
    @PostConstruct
    public void runAfterStartup() {
        roleService.generateRolesToDatabase();
        logger.info("Generate roles to database.");
        userService.createMainAdminUser();
        logger.info("Created administrator.");
    }

}
