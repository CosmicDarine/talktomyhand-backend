package ch.heigvd.talktomyhand.config.security;

import ch.heigvd.talktomyhand.config.security.jwt.AuthenticationEntryPointImpl;
import ch.heigvd.talktomyhand.config.security.jwt.MyAuthTokenFilter;
import ch.heigvd.talktomyhand.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, jsr250Enabled = true, prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	UserService userService;

	@Autowired
	private AuthenticationEntryPointImpl authenticationEntryPointImpl;

	@Bean
	public MyAuthTokenFilter myAuthTokenFilter() {
		return new MyAuthTokenFilter();
	}

	@Override
	public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
		authenticationManagerBuilder.userDetailsService(userService).passwordEncoder(passwordEncoder());
	}

	/**
	 * This is needed in User Service as a Bean.
	 */
	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	/**
	 * This is needed in User Service as a Bean.
	 */
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}



	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.cors()
			.and()
			.csrf().disable()
			.exceptionHandling().authenticationEntryPoint(authenticationEntryPointImpl)
			.and()
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
			.and()
			.authorizeRequests().antMatchers("/auth/**").permitAll()

			.and()
			.authorizeRequests().antMatchers("/**").permitAll()

			// Swagger authorization for DEV
			.and().authorizeRequests().antMatchers("/swagger-resources/**").permitAll()
			.and().authorizeRequests().antMatchers("/swagger-ui/**").permitAll()
			.and().authorizeRequests().antMatchers("/v*/**").permitAll()

			.anyRequest().authenticated()
			.and()
			.addFilterBefore(myAuthTokenFilter(), UsernamePasswordAuthenticationFilter.class);
	}

	//  @Bean
	//  CorsConfigurationSource corsConfigurationSource() {
	//  	CorsConfiguration configuration = new CorsConfiguration();
	//  	configuration.setAllowedOrigins(Arrays.asList("*"));
	//  	configuration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE"));
	//  	UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
	//  	source.registerCorsConfiguration("/**", configuration);
	//  	return source;
	//  }
}
