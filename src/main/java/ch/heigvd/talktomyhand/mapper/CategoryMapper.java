package ch.heigvd.talktomyhand.mapper;

import ch.heigvd.talktomyhand.dto.*;
import ch.heigvd.talktomyhand.model.dialect.sign.Category;
import ch.heigvd.talktomyhand.model.dialect.sign.Sign;
import ch.heigvd.talktomyhand.dto.CategoryCreationDTO;
import ch.heigvd.talktomyhand.dto.CategoryDTO;
import ch.heigvd.talktomyhand.service.SignService;
import net.bytebuddy.dynamic.scaffold.MethodGraph;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Component
public class CategoryMapper extends Mapper<Category, CategoryDTO, CategoryCreationDTO> {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private SignService signService;

    @Autowired
    private SignMapper signMapper;

    @Override
    public Category toModel(CategoryDTO dtoObject) {
        Set<Sign> signs = new HashSet<>();
        for(SignDTO sign : dtoObject.getSigns()) {
            signs.add(signMapper.toModel(sign));
        }

        return Category.builder()
                .idCategory(dtoObject.getId())
                .name(dtoObject.getName())
                .signs(signs)
                .build();
    }

    @Override
    public CategoryDTO toDto(Category modelObject) {

        CategoryDTO categoryDTO = new CategoryDTO();
        List<SignDTO> signs = new LinkedList<>();
        for(Sign sign : modelObject.getSigns()) {
            signs.add(signMapper.toDto(sign));
        }

        categoryDTO.setId((int)modelObject.getIdCategory());
        categoryDTO.setName(modelObject.getName());
        categoryDTO.setSigns(signs);
        return categoryDTO;
    }

    @Override
    public Category toModelFromCreation(CategoryCreationDTO creationObject) {
        return Category.builder()
                .name(creationObject.getName())
                .build();
    }

    public Category toModelFromModification(CategoryModificationDTO categoryDTO) {
        return Category.builder()
                .idCategory(categoryDTO.getId())
                .name(categoryDTO.getName())
                .build();
    }

}
