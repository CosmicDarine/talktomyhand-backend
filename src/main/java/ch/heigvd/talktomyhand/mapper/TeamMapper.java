package ch.heigvd.talktomyhand.mapper;

import ch.heigvd.talktomyhand.dto.TeamCreationDTO;
import ch.heigvd.talktomyhand.dto.TeamDTO;
import ch.heigvd.talktomyhand.dto.TeamModificationDTO;
import ch.heigvd.talktomyhand.dto.UserDTO;
import ch.heigvd.talktomyhand.model.user.Team;
import ch.heigvd.talktomyhand.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Component
public class TeamMapper extends Mapper<Team, TeamDTO, TeamCreationDTO> {
    @Autowired
    private UserMapper usermapper;

    @Override
    public Team toModel(TeamDTO dtoObject) {
        Set<User> users = new HashSet<>();
        for(UserDTO user : dtoObject.getUsers()) {
            users.add(usermapper.toModel(user));
        }

        return Team.builder()
                .idTeam(dtoObject.getId())
                .name(dtoObject.getName())
                .points(dtoObject.getPoints())
                .users(users)
                .build();
    }

    @Override
    public TeamDTO toDto(Team modelObject) {
        TeamDTO teamDTO = new TeamDTO();
        List<UserDTO> users = new LinkedList<>();
        for(User user : modelObject.getUsers()) {
            users.add(usermapper.toDto(user));
        }

        teamDTO.setId((int)modelObject.getIdTeam());
        teamDTO.setName(modelObject.getName());
        teamDTO.setPoints(modelObject.getPoints());
        teamDTO.setUsers(users);
        return teamDTO;
    }

    @Override
    public Team toModelFromCreation(TeamCreationDTO creationObject) {
        return Team.builder()
                .name(creationObject.getName())
                .points(creationObject.getPoints())
                .build();
    }

    public Team toModelFromModification(TeamModificationDTO teamModificationDTO) {
        return Team.builder()
                .idTeam(teamModificationDTO.getId())
                .name(teamModificationDTO.getName())
                .build();
    }
}
