package ch.heigvd.talktomyhand.mapper;

import ch.heigvd.talktomyhand.dto.*;
import ch.heigvd.talktomyhand.model.dialect.sign.Sign;
import ch.heigvd.talktomyhand.model.lesson.Lesson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;


@Component
public class LessonMapper extends  Mapper<Lesson, LessonDTO, LessonCreationDTO> {

    @Autowired
    private SignMapper signMapper;

    @Autowired
    private QuizMapper quizMapper;

    @Override
    public Lesson toModel(LessonDTO dtoObject) {
        Set<Sign> signs = new HashSet<>();
        for(SignDTO sign : dtoObject.getSigns()) {
            signs.add(signMapper.toModel(sign));
        }

        return Lesson.builder()
                .id(dtoObject.getId())
                .name(dtoObject.getName())
                .difficulty(dtoObject.getDifficulty())
                .points(dtoObject.getPoints())
                .signs(signs)
                .quiz(quizMapper.toModel(dtoObject.getQuiz()))
                .build();
    }

    @Override
    public LessonDTO toDto(Lesson modelObject) {
        List<SignDTO> signs = new LinkedList<>();
        for(Sign sign : modelObject.getSigns()) {
            signs.add(signMapper.toDto(sign));
        }

        LessonDTO lessonDTO = new LessonDTO();
        lessonDTO.setId((int) modelObject.getId());
        lessonDTO.setName(modelObject.getName());
        lessonDTO.setDifficulty(modelObject.getDifficulty());
        lessonDTO.setPoints(modelObject.getPoints());
        lessonDTO.setSigns(signs);
        lessonDTO.setQuiz(quizMapper.toDto(modelObject.getQuiz()));
        return lessonDTO;
    }

    @Override
    public Lesson toModelFromCreation(LessonCreationDTO creationObject) {
        Set<Sign> signs = new HashSet<>();
        for(SignDTO sign : creationObject.getSigns()) {
            signs.add(signMapper.toModel(sign));
        }

        return Lesson.builder()
                .name(creationObject.getName())
                .difficulty(creationObject.getDifficulty())
                .points(creationObject.getPoints())
                .signs(signs)
                .quiz(quizMapper.toModel(creationObject.getQuiz()))
                .build();
    }

    public Lesson toModelFromModification(LessonModificationDTO lessonModificationDTO) {
        return Lesson.builder()
                .id(lessonModificationDTO.getId())
                .name(lessonModificationDTO.getName())
                .build();
    }
}
