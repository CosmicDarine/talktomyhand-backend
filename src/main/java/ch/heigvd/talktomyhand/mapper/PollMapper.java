package ch.heigvd.talktomyhand.mapper;

import ch.heigvd.talktomyhand.dto.PollCreationDTO;
import ch.heigvd.talktomyhand.dto.PollDTO;
import ch.heigvd.talktomyhand.dto.PollModificationDTO;
import ch.heigvd.talktomyhand.model.quiz.Poll;
import org.springframework.stereotype.Component;

@Component
public class PollMapper extends Mapper<Poll, PollDTO, PollCreationDTO>{
    @Override
    public Poll toModel(PollDTO dtoObject) {
        return Poll.builder()
                .id(dtoObject.getId())
                .question(dtoObject.getQuestion())
                .questionSign(dtoObject.getQuestionSign())
                .propositions(dtoObject.getPropositions())
                .build();
    }

    @Override
    public PollDTO toDto(Poll modelObject) {
        PollDTO pollDTO = new PollDTO();
        pollDTO.setId(modelObject.getId());
        pollDTO.setQuestion(modelObject.getQuestion());
        pollDTO.setQuestionSign(modelObject.getQuestionSign());
        pollDTO.setPropositions(modelObject.getPropositions());

        return pollDTO;
    }

    @Override
    public Poll toModelFromCreation(PollCreationDTO creationObject) {
        return Poll.builder()
                .question(creationObject.getQuestion())
                .questionSign(creationObject.getQuestionSign())
                .propositions(creationObject.getPropositions())
                .build();
    }


    public Poll toModelFromModification(PollModificationDTO pollModificationDTO) {
        return Poll.builder()
                .id(pollModificationDTO.getId())
                .question(pollModificationDTO.getQuestion())
                .questionSign(pollModificationDTO.getQuestionSign())
                .propositions(pollModificationDTO.getPropositions())
                .build();
    }
}
