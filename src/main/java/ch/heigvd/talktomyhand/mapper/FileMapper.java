package ch.heigvd.talktomyhand.mapper;

import ch.heigvd.talktomyhand.dto.FileCreationDTO;
import ch.heigvd.talktomyhand.dto.FileDTO;
import ch.heigvd.talktomyhand.model.image.File;
import org.springframework.stereotype.Component;

@Component
public class FileMapper extends Mapper<File, FileDTO, FileCreationDTO>{
    @Override
    public File toModel(FileDTO dtoObject) {
        return null;
    }

    //https://stackoverflow.com/questions/6940011/ways-to-convert-string-to-spring-resource
    @Override
    public FileDTO toDto(File modelObject) {
        FileDTO fileDTO = new FileDTO();
        fileDTO.setName(modelObject.getName());
        fileDTO.setId(modelObject.getId());
        return fileDTO;
    }

    @Override
    public File toModelFromCreation(FileCreationDTO creationObject) {
        return null;
    }
}
