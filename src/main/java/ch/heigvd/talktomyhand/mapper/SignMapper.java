package ch.heigvd.talktomyhand.mapper;

import ch.heigvd.talktomyhand.dto.*;
import ch.heigvd.talktomyhand.model.dialect.sign.EDifficulty;
import ch.heigvd.talktomyhand.model.dialect.sign.Sign;
import org.springframework.stereotype.Component;

@Component
public class SignMapper extends Mapper<Sign, SignDTO, SignCreationDTO> {

    @Override
    public Sign toModel(SignDTO dtoObject) {
        return Sign.builder()
                .id(dtoObject.getId())
                .description(dtoObject.getDescription())
                .traduction(dtoObject.getTraduction())
                //.videoID(dtoObject.getVideo())
                .difficulty(EDifficulty.getDifficulty(dtoObject.getDifficulty()))
                .build();
    }

    @Override
    public SignDTO toDto(Sign modelObject) {
        SignDTO signDTO = new SignDTO();
        signDTO.setId((int)modelObject.getId());
        signDTO.setDescription(modelObject.getDescription());
        signDTO.setTraduction(modelObject.getTraduction());
        signDTO.setDifficulty(modelObject.getDifficulty().name());
        //signDTO.setVideo(modelObject.getVideoID());

        return signDTO;
    }

    @Override
    public Sign toModelFromCreation(SignCreationDTO creationObject) {
        return Sign.builder()
                .traduction(creationObject.getTraduction())
                .description(creationObject.getDescription())
                .difficulty(EDifficulty.getDifficulty(creationObject.getDifficulty()))
                //.videoID(creationObject.getVideo())
                .build();
    }




    public Sign toModelFromModification(SignModificationDTO signModificationDTO) {
        return Sign.builder()
                .id(signModificationDTO.getId())
                .description(signModificationDTO.getDescription())
                .traduction(signModificationDTO.getTraduction())
                .difficulty(EDifficulty.getDifficulty(signModificationDTO.getDifficulty()))
                //.videoID(signModificationDTO.getVideo())
                .build();
    }
}
