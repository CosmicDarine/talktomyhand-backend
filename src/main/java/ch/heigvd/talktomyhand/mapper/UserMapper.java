package ch.heigvd.talktomyhand.mapper;

import ch.heigvd.talktomyhand.dto.*;
import ch.heigvd.talktomyhand.model.dialect.sign.Category;
import ch.heigvd.talktomyhand.model.lesson.Lesson;
import ch.heigvd.talktomyhand.model.user.User;
import ch.heigvd.talktomyhand.model.user.role.Role;
import ch.heigvd.talktomyhand.model.user.sex.ESex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Link;
import java.util.*;

@Component
public class UserMapper extends Mapper<User, UserDTO, UserDTO> {
    @Autowired
    private RoleMapper roleMapper;

    @Autowired
    private LessonMapper lessonMapper;


    @Override
    public User toModel(UserDTO dtoObject) {
        List<Role> roleList = new LinkedList<>();
        dtoObject.getRoles().forEach(roleDTO -> {
            roleList.add(roleMapper.toModel(roleDTO));
        });

        Set<Lesson> lessonList = new HashSet<>();
        dtoObject.getCompletedLessons().forEach(lessonDTO -> {
            lessonList.add(lessonMapper.toModel(lessonDTO));
        });

        return User.builder()
                .idUser(dtoObject.getIdUser())
                .username(dtoObject.getUsername())
                .firstname(dtoObject.getFirstname())
                .lastname(dtoObject.getLastname())
                .birthdate(dtoObject.getBirthdate())
                .email(dtoObject.getEmail())
                .password(dtoObject.getPassword())
                .sex(ESex.getSex(dtoObject.getSex()))
                .roles(roleList)
                .points(dtoObject.getPoints())
                .level(dtoObject.getLevel())
                .completedLessons(lessonList)
                .build();
    }

    @Override
    public UserDTO toDto(User modelObject) {
        UserDTO userDTO = new UserDTO();
        userDTO.setIdUser((int) modelObject.getIdUser());
        userDTO.setUsername(modelObject.getUsername());
        userDTO.setFirstname(modelObject.getFirstname());
        userDTO.setLastname(modelObject.getLastname());
        userDTO.setBirthdate(modelObject.getBirthdate());
        userDTO.setEmail(modelObject.getEmail());
        userDTO.setPassword(modelObject.getPassword());
        userDTO.setSex(modelObject.getSex().name());
        userDTO.setPoints(modelObject.getPoints());
        userDTO.setLevel(modelObject.getLevel());
        //userDTO.setImage(imageMapper.toDto(modelObject.getProfilePic()));

        List<RoleDTO> roleList = new LinkedList<>();
        modelObject.getRoles().forEach(role -> {
            roleList.add(roleMapper.toDto(role));
        });
        userDTO.setRoles(roleList);

        List<LessonDTO> lessonList = new LinkedList<>();
        if(modelObject.getCompletedLessons() != null) {
            modelObject.getCompletedLessons().forEach(lesson -> {
                lessonList.add(lessonMapper.toDto(lesson));
            });
        }

        userDTO.setCompletedLessons(lessonList);
        return userDTO;
    }

    @Override
    public User toModelFromCreation(UserDTO creationObject) {
        return null;
    }

    /**
     * Map a dto register object to it's user model equivalent
     * @param registerDTO the dto register object to map
     * @return userModel the user model created from the given dto register object
     */
    public User toModel(RegisterDTO registerDTO) {
        return User.builder()
                .username(registerDTO.getUsername())
                .firstname(registerDTO.getFirstname())
                .lastname(registerDTO.getLastname())
                .birthdate(registerDTO.getBirthdate())
                .email(registerDTO.getEmail())
                .password(registerDTO.getPassword())
                .sex(ESex.getSex(registerDTO.getSex()))
                .build();
    }

    /**
     * Map
     * @param user
     * @param jwt
     * @return
     */
    public LoginSuccessDTO toLoginSuccessDTO(User user, String jwt) {
        LoginSuccessDTO loginSuccessDTO = new LoginSuccessDTO();
        loginSuccessDTO.setJwt(jwt);
        loginSuccessDTO.setUsername(user.getUsername());
        List<String> roles = new LinkedList<>();
        user.getRoles().forEach(role -> {
            roles.add(role.getName().toString());
        });

        loginSuccessDTO.setRoles(roles);
        return loginSuccessDTO;
    }

    public UserSimpleDTO toSimple(User user) {
        UserSimpleDTO userDTO = new UserSimpleDTO();
        userDTO.setUsername(user.getUsername());
        userDTO.setFirstname(user.getFirstname());
        userDTO.setLastname(user.getLastname());
        userDTO.setBirthdate(user.getBirthdate());
        userDTO.setSex(user.getSex().name());
        userDTO.setPoints(user.getPoints());
        userDTO.setLevel(user.getLevel());
        userDTO.setEmail(user.getEmail());

        List<LessonDTO> lessonList = new LinkedList<>();
        user.getCompletedLessons().forEach(lesson -> {

        });

        return userDTO;
    }

    public User toModelFromModification(UserModificationDTO userModificationDTO) {

        return User.builder()
                .idUser(userModificationDTO.getIdUser())
                .username(userModificationDTO.getUsername())
                .firstname(userModificationDTO.getFirstname())
                .lastname(userModificationDTO.getLastname())
                .birthdate(userModificationDTO.getBirthdate())
                .sex(ESex.getSex(userModificationDTO.getSex()))
                .email(userModificationDTO.getEmail())
                .points(userModificationDTO.getPoints())
                .level(userModificationDTO.getLevel())
                .password(userModificationDTO.getPassword())
                .build();
    }

    /*
    public User toModelFromModificationLesson(UserAddPointsForLessonDTO userAddPointsForLessonDTO) {
        Set<Lesson> lessons = new HashSet<>();
        userAddPointsForLessonDTO.getCompletedLessons().forEach(lessonDTO -> {
            lessons.add(lessonMapper.toModel(lessonDTO));
        });

        //Set<Role> roles = new HashSet<>();

        return User.builder()
                .idUser(userAddPointsForLessonDTO.getIdUser())
                .points(userAddPointsForLessonDTO.getPoints())
                .level(userAddPointsForLessonDTO.getLevel())
                .completedLessons(lessons)
                .build();
    }

     */

    public User toModelFromSimple(UserSimpleDTO userDTO) {
        //TODO : correct dat shit

        /*
        Set<Lesson> completedLessonsList = new LinkedList<>();
        if(userDTO.getCompletedLessons() != null || !userDTO.getCompletedLessons().isEmpty()) {
            userDTO.getCompletedLessons().forEach(completedLessonDTO -> {
                Lesson lesson = new Lesson();
                lesson.setId(completedLessonDTO.getId());
                lesson.setName(completedLessonDTO.getName());
                lesson.setDifficulty(completedLessonDTO.getDifficulty());
                lesson.setReward(completedLessonDTO.getReward());
                Set<User> subscribersList = new HashSet<>();
                completedLessonDTO.getSubscribers().forEach(subscriber -> {
                    subscribersList.add()
                });
                lesson.setSubscribers(completedLessonDTO.getSubscribers());
                completedLessonsList.add(lesson);
            });
        }*/

        return User.builder()
                .username(userDTO.getUsername())
                .firstname(userDTO.getFirstname())
                .lastname(userDTO.getLastname())
                .birthdate(userDTO.getBirthdate())
                .sex(ESex.getSex(userDTO.getSex()))
                .points(userDTO.getPoints())
                .level(userDTO.getLevel())
                //.completedLessons(completedLessonsList)
                .email(userDTO.getEmail())
                .password("")
                .build();
    }
}
