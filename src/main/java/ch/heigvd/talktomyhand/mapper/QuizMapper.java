package ch.heigvd.talktomyhand.mapper;

import ch.heigvd.talktomyhand.dto.*;
import ch.heigvd.talktomyhand.model.dialect.sign.Sign;
import ch.heigvd.talktomyhand.model.quiz.Poll;
import ch.heigvd.talktomyhand.model.quiz.Quiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Component
public class QuizMapper extends Mapper<Quiz, QuizDTO, QuizCreationDTO>{

    @Autowired
    private SignMapper signMapper;

    @Autowired
    private PollMapper pollMapper;

    @Override
    public Quiz toModel(QuizDTO dtoObject) {
        Set<Poll> polls = new HashSet<>();

        for(PollDTO poll : dtoObject.getPolls()) {
            polls.add(pollMapper.toModel(poll));
        }
        return Quiz.builder()
                .id(dtoObject.getId())
                .name(dtoObject.getName())
                .polls(polls)
                .build();
    }

    @Override
    public QuizDTO toDto(Quiz modelObject) {
        QuizDTO quizDTO = new QuizDTO();
        List<PollDTO> polls = new LinkedList<>();

        for(Poll poll : modelObject.getPolls()) {
            polls.add(pollMapper.toDto(poll));
        }

        quizDTO.setId((int)modelObject.getId());
        quizDTO.setName(modelObject.getName());
        quizDTO.setPolls(polls);

        return quizDTO;
    }

    @Override
    public Quiz toModelFromCreation(QuizCreationDTO creationObject) {
        return Quiz.builder()
                .name(creationObject.getName())
                .build();
    }

    public Quiz toModelFromModification(QuizModificationDTO quizModificationDTO) {
        return Quiz.builder()
                .id(quizModificationDTO.getId())
                .name(quizModificationDTO.getName())
                .build();
    }
}
