package ch.heigvd.talktomyhand.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import ch.heigvd.talktomyhand.dto.RoleDTO;
import ch.heigvd.talktomyhand.model.user.role.ERole;
import ch.heigvd.talktomyhand.model.user.role.Role;
import org.openapitools.jackson.nullable.JsonNullable;
import org.springframework.stereotype.Component;

@Component
public class RoleMapper extends Mapper<Role, RoleDTO, RoleDTO> {
    /**
     *
     * @param roles
     * @return
     */
    public List<String> toStringRoles(Set<Role> roles) {
        List<String> stringRoles = new ArrayList<>();
        for (Role role : roles) {
            stringRoles.add(toStringRole(role));
        }
        return stringRoles;
    }

    /**
     *
     * @param role
     * @return
     */
    private String toStringRole(Role role) {
        return role.getName().toString();
    }

    @Override
    public Role toModel(RoleDTO dtoObject) {
        return Role.builder()
                .roleId(dtoObject.getId())
                .name(ERole.getRole(dtoObject.getName()))
                .build();
    }

    @Override
    public RoleDTO toDto(Role modelObject) {
        RoleDTO roleDTO = new RoleDTO();
        roleDTO.setId((int)modelObject.getRoleId());
        roleDTO.setName(modelObject.getName().name());
        return roleDTO;
    }

    @Override
    public Role toModelFromCreation(RoleDTO creationObject) {
        return null;
    }
}
