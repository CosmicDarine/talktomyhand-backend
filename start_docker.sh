#!/usr/bin/env bash

# shellcheck disable=SC2164
cd docker/topologies/

docker-compose pull

docker-compose up --build

